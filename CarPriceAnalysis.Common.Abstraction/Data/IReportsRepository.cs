﻿using CarPriceAnalysis.Common.Model.Data;
using System;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface IReportsRepository
    {
        Task<ReportRequest> FindRequestByIdAsync(Guid id);
        Task SaveRequestAsync(ReportRequest report);
        Task UpdateRequestAsync(ReportRequest report);
        Task SaveReportAsync(ReportDetails report);
        Task<ReportDetails> FindReportByReportId(Guid reportId);

    }
}
