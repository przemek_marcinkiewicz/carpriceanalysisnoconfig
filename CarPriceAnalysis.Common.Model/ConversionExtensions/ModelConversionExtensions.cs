﻿using CarPriceAnalysis.Common.Model.Business;
using CarPriceAnalysis.Common.Model.Data;

namespace CarPriceAnalysis.Common.Model.ConversionExtensions
{
    public static class ModelConversionExtensions
    {
        public static AzureMLRequest ToAzureMLRequest(this ReportRequest report)
        {
            return new AzureMLRequest
            {
                OfferId = 0,
                Make = report.Make,
                Model = report.Model,
                Version = report.Version,
                BodyType = report.BodyType,
                GearingType = report.GearingType,
                FuelType = report.FuelType,
                Price = 0,
                ProductionYear = report.ProductionYear,
                Mileage = report.Mileage,
                Displacement = report.Displacement,
                Power = report.Power,
                OriginCountry = report.OriginCountry,
                Color = report.Color,
                DoorsCount = report.DoorsCount,

                IsBroken = report.IsBroken,
                IsFirstOwner = report.IsFirstOwner,
                IsStartStop = report.IsStartStop,
                IsAccidentFree = report.IsAccidentFree,
                IsEnglishVersion = report.IsEnglishVersion,

                IsNavi = report.IsNavi,
                IsTempomat = report.IsTempomat,
                IsXenon = report.IsXenon,
                IsLeather = report.IsLeather,
                IsAutomaticClima = report.IsAutomaticClima,
                IsManualClima = report.IsManualClima,
                IsSeatHeating = report.IsSeatHeating,
                IsMultifunctionalSteeringWheel = report.IsMultifunctionalSteeringWheel

            };
        }
    }
}
