﻿namespace CarPriceAnalysis.Common.Model.Data.Enums
{
    public enum PromoCodeType
    {
        One = 1,
        Multiple = 2
    }
}
