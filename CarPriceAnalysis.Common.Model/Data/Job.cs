﻿using CarPriceAnalysis.Common.Model.Data.Enums;
using System;

namespace CarPriceAnalysis.Common.Model.Data
{
    public class Job
    {
        public long Id { get; set; }
        public JobName ActionName { get; set; }
        public string ActionParameters { get; set; }

        public DateTime EnqueueDate { get; set; }
        public DateTime? ProcessingDate { get; set; }
        public JobResult ProcessingResult { get; set; }
    }
}
