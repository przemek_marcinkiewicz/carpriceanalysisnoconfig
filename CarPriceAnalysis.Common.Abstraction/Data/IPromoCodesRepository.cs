﻿using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using System;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface IPromoCodesRepository
    {
        Task<PromoCode> GetByCode(string code);
        Task UpdateAsync(PromoCode code);
        Task<PromoCode> Create(PromoCodeType type, double? priceSingle = 0, double? priceMultiple = 0, DateTime? expirationDate = null);
    }
}
