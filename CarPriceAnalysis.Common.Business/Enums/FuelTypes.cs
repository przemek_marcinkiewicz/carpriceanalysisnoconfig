﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum FuelTypes
    {
        [Description("Benzyna")]
        Gasoline,

        [Description("Benzyna + LPG")]
        GasolineLPG,

        [Description("Diesel")]
        Diesel,

        [Description("Elektryczny")]
        Electric,

        [Description("Hybryda")]
        Hybrid
    }
}
