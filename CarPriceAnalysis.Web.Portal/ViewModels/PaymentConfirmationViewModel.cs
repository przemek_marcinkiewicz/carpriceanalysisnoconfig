﻿namespace CarPriceAnalysis.Web.Portal.ViewModels
{
    public class PaymentConfirmationViewModel
    {
        public string operation_number { get; set; }
        public string operation_status { get; set; }
        public string operation_amount { get; set; }
        public string operation_commission_amount { get; set; }
        public string operation_original_amount { get; set; }
        public string operation_datetime { get; set; }
        public string description { get; set; }
        public string email { get; set; }
    }
}
