﻿using AutoMapper;
using CarPriceAnalysis.Common.Abstraction.Business;
using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Web.Portal.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CarPriceAnalysis.Web.Portal.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {

        private ICarDetailsRepository _repository;
        private IAzureMLService _azureMLService;
        private IReportsRepository _reportsRepository;
        private IMapper _mapper;

        public HomeController(ICarDetailsRepository repository, IAzureMLService azureMLService, IReportsRepository reportsRepository, IMapper mapper)
        {
            _repository = repository;
            _azureMLService = azureMLService;
            _reportsRepository = reportsRepository;
            _mapper = mapper;
        }

        [HttpGet("")]
        public IActionResult Index(string promoCode)
        {
            var carDetails = new CarDetailsViewModel()
            {
                PromoCode = promoCode,
                MakesList = _repository.GetBrands()
            };
            return View(carDetails);
        }
    }
}
