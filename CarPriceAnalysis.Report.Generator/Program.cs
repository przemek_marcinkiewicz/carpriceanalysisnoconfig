﻿using CarPriceAnalysis.Common.Data;
using CarPriceAnalysis.Common.Data.Repositories;
using CarPriceAnalysis.Common.Model.Data;
using System.Threading.Tasks;
using System.Linq;
using System;
using CarPriceAnalysis.Common.Model.ConversionExtensions;
using CarPriceAnalysis.Common.Business.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace CarPriceAnalysis.Report.Generator
{
    public class Program
    {
        private static ILogger<Program> _logger;
        public static void Main(string[] args)
        {

            Configure();

            //Replace with DI
            var dbContextFactory = new AppDbContextFactory();
            var db = dbContextFactory.Create();
            var offersRepository = new OffersRepository(db);
            var reportsRepository = new ReportsRepository(db);
            var carDetailsRepository = new CarDetailsRepository(db);
            var queue = new QueueRepository(db);
            var azureMLServiceDetailsRepository = new AzureMLServicesRepository();

            Task.Run(async () =>
            {
                try
                {
                    var nextJob = await queue.GetNext(Common.Model.Data.Enums.JobName.GenerateReport);
                    if (nextJob != null)
                    {
                        _logger.LogInformation("START JOB EXECUTION");
                        _logger.LogInformation("Getting next report request");
                        var nextReport = await reportsRepository.FindRequestByIdAsync(new Guid(nextJob.ActionParameters));
                        _logger.LogInformation("Next report request", nextReport);

                        var allCarsNumber = await offersRepository.GetAllOffersCount();
                        var cars = offersRepository.GetCarsQuery(nextReport.Make, nextReport.Model, nextReport.Version);
                        _logger.LogInformation($"Downloaded [{cars.Count()}] car offers.");
                        var productionYears = carDetailsRepository.GetYears(nextReport.Make, nextReport.Model, nextReport.Version);
                        _logger.LogInformation($"Extracted production years.");

                        //CALCULATING STATISTICS
                        var report = new ReportDetails();
                        report.NumberOfAllOffers = allCarsNumber;

                        var serviceDetails = azureMLServiceDetailsRepository.GetServiceDetails(nextReport.Make);
                        var azureMLService = new AzureMLService(serviceDetails);
                        
                        var miningModel = nextReport.ToAzureMLRequest();
                        var azureMLResult = await azureMLService.GetPricePrediction(miningModel);
                        _logger.LogInformation($"AzureML Prediction", azureMLResult);

                        //Car: Price | Azure ML
                        report.Price = azureMLResult.Price;

                        //Car: Count | Allegro
                        var carsCount = cars.Count();
                        var carsTrimmed = cars.Skip(10).Take(carsCount - 20);
                        var carsTrimmedCount = carsTrimmed.Count();

                        report.NumberOfSamples = carsCount;

                        //Car: AvgPrice | Allegro
                        report.AveragePrice = carsTrimmed
                        .Average(x => x.Price);

                        //Car: Avg Mileage | Allegro
                        report.AvgMileage = carsTrimmed
                        .Where(c => c.Mileage != null)
                        .Select(c => int.Parse(c.Mileage))
                        .Average(avg => avg);

                        //Car: Avg Price at the same year | Allegro
                        var carsSameYear = carsTrimmed.Where(x => x.ProductionYear == nextReport.ProductionYear);
                        report.AvgPriceSameYear = carsSameYear.Any() ? carsSameYear.Average(x => x.Price) : 0;


                        //Car: Avg Price at the same year and similiar mileage | Allegro
                        var carsSameyearSimiliarMielage = carsTrimmed.Where(c => c.Mileage != null)
                        .Where(x => x.ProductionYear == nextReport.ProductionYear && int.Parse(x.Mileage) - 20000 < int.Parse(nextReport.Mileage) && int.Parse(x.Mileage) + 20000 > int.Parse(nextReport.Mileage));

                        report.AvgPriceSameYearSimiliarMileage = carsSameyearSimiliarMielage.Any() ? carsSameyearSimiliarMielage.Average(x => x.Price) : 0;

                        //Car: Percent of higher prices of cars NOT depending on year | Allegro
                        report.PercentOfHigherPricesAtDifferentYears = ((double)carsTrimmed.Count(c => c.Price > azureMLResult.Price) / carsTrimmedCount) * 100;

                        //Car: Percent of higher prices of cars depending on year | Allegro
                        report.PercentOfHigherPricesAtSameYear = ((double)carsTrimmed.Where(x => x.ProductionYear == nextReport.ProductionYear).Count(c => c.Price > azureMLResult.Price) / carsTrimmedCount) * 100;

                        //Car: Percent of features | Allegro
                        report.PercentOfIsAutomaticClima = ((double)carsTrimmed.Where(c => c.IsAutomaticClima).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsLeather = ((double)carsTrimmed.Where(c => c.IsLeather).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsManualClima = ((double)carsTrimmed.Where(c => c.IsManualClima).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsMultifunctionalSteeringWheel = ((double)carsTrimmed.Where(c => c.IsMultifunctionalSteeringWheel).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsNavi = ((double)carsTrimmed.Where(c => c.IsNavi).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsSeatHeating = ((double)carsTrimmed.Where(c => c.IsSeatHeating).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsStartStop = ((double)carsTrimmed.Where(c => c.IsStartStop).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsTempomat = ((double)carsTrimmed.Where(c => c.IsTempomat).Count() / carsTrimmedCount) * 100;
                        report.PercentOfIsXenon = ((double)carsTrimmed.Where(c => c.IsXenon).Count() / carsTrimmedCount) * 100;

                        //Car: Chart: Avg price - year | Allegro
                        report.ChartAvgPriceToYear = string.Join(";", productionYears.Where(p => carsTrimmed.Any(c => c.ProductionYear == p)).Select(p => new { price = carsTrimmed.Where(c => c.ProductionYear == p)?.Average(y => y.Price), year = p }).Select(pair => string.Format("{0}:{1}", pair.year, pair.price)));

                        //Skip 5% of edge values
                        int skipChunk = (int)(carsCount / 20);

                        //Car: Chart: All prices ordered depending on car year | Allegro

                        var pricesOdrered = cars.Select(c => c.Price).OrderBy(p => p).Skip(skipChunk).Take(carsCount - (skipChunk * 2));
                        report.ChartPricesOrdered = string.Join(";", pricesOdrered.GroupBy(p => Round(p, 10000)).Select(pair => string.Format("{0}:{1}", pair.Key, pair.Count())));

                        if (carsSameYear.Any())
                        {
                            var pricesSameYearOdrered = carsSameYear.Select(c => c.Price).OrderBy(p => p).Skip(skipChunk).Take(carsCount - (skipChunk * 2));
                            report.ChartPricesSameYearOrdered = string.Join(";", pricesSameYearOdrered.GroupBy(p => Round(p, 1000)).Select(pair => string.Format("{0}:{1}", pair.Key, pair.Count())));
                        }

                        _logger.LogInformation($"Save", report);
                        //Save
                        report.CreateDateTime = DateTime.UtcNow;
                        report.ReportId = nextReport.Id;
                        await reportsRepository.SaveReportAsync(report);

                        nextReport.GenerateDateTime = report.CreateDateTime;
                        nextReport.AccessCode = Guid.NewGuid();

                        _logger.LogInformation($"Update access code");
                        await reportsRepository.UpdateRequestAsync(nextReport);

                        _logger.LogInformation($"Update job status");
                        await queue.UpdateStatus(nextJob, Common.Model.Data.Enums.JobResult.Success);

                        _logger.LogInformation($"Enqueue report publish {report.ReportId}");
                        await queue.Enqueue(Common.Model.Data.Enums.JobName.PublishReport, report.ReportId.ToString());
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("Exception", ex);
                }
            }).Wait();
        }

        private static int Round(double i, int nearest)
        {
            return (int)(Math.Round(i / 1000d, 0) * 1000);
        }

        private static void Configure()
        {
            //setup our DI
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .BuildServiceProvider();

            //configure console logging
            serviceProvider
                .GetService<ILoggerFactory>()
                .AddFile("Logs/log-{Date}.txt");

            _logger = serviceProvider.GetService<ILoggerFactory>()
                .CreateLogger<Program>();
        }
    }
}
