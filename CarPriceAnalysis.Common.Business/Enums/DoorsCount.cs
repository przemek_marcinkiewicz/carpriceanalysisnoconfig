﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum DoorsCount
    {
        [Description("2-3")]
        TwoThree,

        [Description("4-5")]
        FourFive
    }
}
