﻿using AutoMapper;
using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Web.Portal.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Web.Portal.Controllers
{
    [Route("Payment")]
    public class PaymentController : Controller
    {
        private IReportsRepository _reportsRepository;
        private IQueueRepository _queue;
        private IPaymentsRepository _paymentRepository;
        private IMapper _mapper;

        public PaymentController(IReportsRepository reportsRepository, IPaymentsRepository paymentRepository, IQueueRepository queue, IMapper mapper)
        {
            _queue = queue;
            _reportsRepository = reportsRepository;
            _paymentRepository = paymentRepository;
            _mapper = mapper;
        }

        [HttpPost("Dotpay/Confirmation")]
        public async Task<IActionResult> Confirm(PaymentConfirmationViewModel viewModel)
        {

            var payment = await _paymentRepository.GetByDescription(viewModel.description);
            if (payment.Status == Common.Model.Data.Enums.PaymentStatus.Completed)
            {
                return View();
            }
            payment.Amount = double.Parse(viewModel.operation_amount, CultureInfo.InvariantCulture);
            //payment.Commision = double.Parse(viewModel.operation_commission_amount, CultureInfo.InvariantCulture);
            payment.Email = viewModel.email;
            payment.OperationDateTime = DateTime.ParseExact(viewModel.operation_datetime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            payment.OperationNumber = viewModel.operation_number;
            payment.Status = viewModel.operation_status == "completed" ? Common.Model.Data.Enums.PaymentStatus.Completed : Common.Model.Data.Enums.PaymentStatus.Rejected;
            payment.ConfirmationDateTime = DateTime.UtcNow;
            payment.OriginalAmount = double.Parse(viewModel.operation_original_amount, CultureInfo.InvariantCulture);

            await _paymentRepository.UpdateAsync(payment);

            if (payment.Status == Common.Model.Data.Enums.PaymentStatus.Completed)
            {
                await _queue.Enqueue(Common.Model.Data.Enums.JobName.GenerateReport, payment.ReportRequestId.ToString());
            }
                return View();
        }
    }
}
