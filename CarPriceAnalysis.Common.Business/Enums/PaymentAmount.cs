﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum PaymentAmount
    {
        [Description("2.99")]
        Basic = 1,

        [Description("6.99")]
        Premium = 3
    }
}