$ProgressPreference="SilentlyContinue"
$WarningPreference = "SilentlyContinue"
$ErrorActionPreference = "SilentlyContinue"

$sqlConnection = new-object System.Data.SqlClient.SqlConnection
$sqlConnection.ConnectionString = ""
$sqlConnection.Open()
Write-Output "Connection opened."

$checkCommand = $sqlConnection.CreateCommand()
#Get processing unit Id for ClearDuplicates ONLY if no GetOffer jobs left unprocessed
$checkCommand.CommandText = "  SELECT TOP 1 [Id], [ActionParameters] from dbo.JobsQueue where ActionName like '2' and ProcessingDate is null order by EnqueueDate"

$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $checkCommand
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
$sqlConnection.Close()

#Check if any ClearDuplicates job is waiting to be processed
if ($DataSet.Tables[0].Rows.Count -eq 0)
{ 	
 	Write-Output "Nothing to do. EXIT"
}
else
{

	#Get reportId
	$jobId = $DataSet.Tables[0].Rows[0][0]
	Write-Output "Found processing unit. JobId:" $jobId

	$reportId = $DataSet.Tables[0].Rows[0][1]
	Write-Output "Found processing unit. ReportId:" $reportId

	$pdfFileName = $reportId + ".pdf";
	
	
	#GenerateWith WKHTMLTOPDF
	$wkhtmlLocation = $PSScriptRoot + "\wkhtmltopdf\wkhtmltopdf.exe"
	$reportUrl = "http://autocena.azurewebsites.net/pdf/"+ $reportId
	$wkhtmlParams = "--pageSize A4 --dpi 300 --zoom 0.7 --disable-smart-shrinking"

	& $wkhtmlLocation `--page-size A4 `--dpi 300 `--zoom 0.7 `--disable-smart-shrinking  $reportUrl $pdfFileName

	# #Upload to Azure Storage
	$storageAccountName = ""
	$storageAccountKey = ""
	$containerName = ""

	# Write-Output "Creating PDF File"
	 $fullFileName = $PSScriptRoot + "\" + $pdfFileName

	# Write-Output "Uploading PDF File to Azure Storage (" $fullFileName ")"
	 $ProgressPreference="SilentlyContinue"
	 $ctx = New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $storageAccountKey -Verbose
	 Set-AzureStorageBlobContent -File "$fullFileName" -Container $containerName -Blob $pdfFileName -Context $ctx -Verbose -Force

	# #Update Processing Unit Execution Details
	Write-Output "Updating Processing Unit status"
	$sqlConnection.Open()
	$checkCommand = $sqlConnection.CreateCommand()
	$checkCommand.CommandText = "UPDATE dbo.JobsQueue set ProcessingDate=GetUtcDate(), ProcessingResult='1' where Id="+$jobId;
	$resultA = $checkCommand.ExecuteNonQuery()
	Write-Output "Result:" $resultA
	$sqlConnection.Close()

	# #Enqueue sending email
	Write-Output "Creating new Processing Unit to send email"
	$sqlConnection.Open()
	$checkCommand = $sqlConnection.CreateCommand()
	$checkCommand.CommandText = "insert into [dbo].[JobsQueue] values ('3','"+$reportId+"',GetUtcDate(),null,0)";
	$resultA = $checkCommand.ExecuteNonQuery()
	Write-Output "Result:" $resultA
	$sqlConnection.Close()
 }

