﻿namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum QueueActionName
    {
        GenerateReport,
        PublishReport,
        SendEmail,
        GetOffers,
        ClearDuplicates
    }
}
