﻿using CarPriceAnalysis.Common.Model.Business;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Business
{
    public interface IAzureMLService
    {
        void SetAzureMLServiceDetails(AzureMLServiceDetails service);
        Task<AzureMLPrediction> GetPricePrediction(AzureMLRequest service);
    }
}
