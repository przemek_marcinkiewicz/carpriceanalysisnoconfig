﻿using CarPriceAnalysis.Common.Abstraction.Business;
using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Common.Business.Services;
using CarPriceAnalysis.Common.Data;
using CarPriceAnalysis.Common.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CarPriceAnalysis.Web.Portal.Configuration
{
    public static class ServicesConfiguration
    {
        internal static void AddDatabase(this IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer());
        }

        internal static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<ICarDetailsRepository, CarDetailsRepository>();
            services.AddScoped<IReportsRepository, ReportsRepository>();
            services.AddScoped<IAzureMLService, AzureMLService>();
            services.AddScoped<IAzureStorageProxyService, AzureStorageProxyService>();
            services.AddScoped<IQueueRepository, QueueRepository>();
            services.AddScoped<IPaymentsRepository, PaymentsRepository>();
            services.AddScoped<IPromoCodesRepository, PromoCodesRepository>();
            services.AddScoped<IAzureMLServicesRepository, AzureMLServicesRepository>();
        }
    }
}
