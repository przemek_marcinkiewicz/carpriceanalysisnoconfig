$sqlConnection = new-object System.Data.SqlClient.SqlConnection
$sqlConnection.ConnectionString = ""
$sqlConnection.Open()
Write-Output "Connection opened."

$checkCommand = $sqlConnection.CreateCommand()
#Get processing unit Id for ClearDuplicates ONLY if no GetOffer jobs left unprocessed
$checkCommand.CommandText = "  SELECT Id from dbo.JobsQueue where ActionName like '5' and ProcessingDate is null and (SELECT COUNT(1) from dbo.JobsQueue where ActionName like '4' and ProcessingDate is null) = 0"

$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
$SqlAdapter.SelectCommand = $checkCommand
$DataSet = New-Object System.Data.DataSet
$SqlAdapter.Fill($DataSet)
$sqlConnection.Close()

#Check if any ClearDuplicates job is waiting to be processed
if ($DataSet.Tables[0].Rows.Count -eq 0)
{ 	
 	Write-Output "Nothing to do. EXIT"
}
else
{
	$jobId = $DataSet.Tables[0].Rows[0][0]
	Write-Output "Found processing unit. JobId:" $jobId
	#Execute Duplicates Cleaning Procedure
	$sqlConnection.Open()
	$sqlCommand = $sqlConnection.CreateCommand()
	$sqlCommand.CommandTimeout = 120
	$sqlCommand.Connection = $sqlConnection
	$sqlCommand.CommandText= "EXEC proc_RemoveDuplicates"
	Write-Output "Query execution ready."
	$result = $sqlCommand.ExecuteNonQuery()
	Write-Output "Removed duplicates:" $result
	$sqlConnection.Close()
	Write-Output "Connection closed. EXIT"

	#Update Processing Unit Execution Details
	$sqlConnection.Open()
	$checkCommand = $sqlConnection.CreateCommand()
	$checkCommand.CommandText = "UPDATE dbo.JobsQueue set ProcessingDate=GetUtcDate(), ProcessingResult='1' where Id="+$jobId;
	$resultA = $checkCommand.ExecuteNonQuery()
	Write-Output "Result:" $resultA
	$sqlConnection.Close()
 }

