﻿using CarPriceAnalysis.Common.Business.Enums;
using CarPriceAnalysis.Common.Business.Extensions;
using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using CarPriceAnalysis.Web.Portal.Infrastructure.ModelBinders.FromYesNo;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarPriceAnalysis.Web.Portal.ViewModels
{
    public class CarDetailsViewModel
    {

        #region Model Properties

        [Display(Name = "Marka")]
        [Required(ErrorMessage = "Wybierz markę pojazdu")]
        public string Make { get; set; }

        [Display(Name = "Model")]
        [Required(ErrorMessage = "Wybierz model pojazdu")]
        public string Model { get; set; }

        [Display(Name = "Wersja")]
        public string Version { get; set; }

        [Display(Name = "Typ nadwozia")]
        [Required(ErrorMessage = "Wybierz rodzaj nadwozia")]
        public string BodyType { get; set; }

        [Display(Name = "Rodzaj skrzyni biegów")]
        [Required(ErrorMessage = "Wybierz rodzaj skrzyni biegów")]
        public string GearingType { get; set; }

        [Display(Name = "Rodzaj paliwa")]
        [Required(ErrorMessage = "Wybierz rodzaj paliwa")]
        public string FuelType { get; set; }

        [Display(Name = "Cena")]
        public double? Price { get; set; }

        [Display(Name = "Rok produkcji")]
        [Required(ErrorMessage = "Wybierz rok produkcji")]
        public string ProductionYear { get; set; }

        [Display(Name = "Przebieg (km)")]
        [Required(ErrorMessage = "Podaj przebieg")]
        public int? Mileage { get; set; }

        [Display(Name = "Pojemność silnika (cm3)")]
        [Required(ErrorMessage = "Podaj pojemność silnika")]
        public string Displacement { get; set; }

        [Display(Name = "Moc (KM)")]
        [Required(ErrorMessage = "Podaj moc")]
        public int? Power { get; set; }

        [Display(Name = "Kraj pochodzenia")]
        [Required(ErrorMessage = "Wybierz kraj pochodzenia")]
        public string OriginCountry { get; set; }

        [Display(Name = "Kolor")]
        [Required(ErrorMessage = "Wybierz kolor")]
        public string Color { get; set; }

        [Display(Name = "Liczba drzwi")]
        [Required(ErrorMessage = "Wybierz liczbę drzwi")]
        public string DoorsCount { get; set; }

        [FromYesNo]
        [Display(Name = "Uszkodzony")]
        [Required(ErrorMessage = "Wybierz właściwą opcję")]
        public bool IsBroken { get; set; }

        [FromYesNo]
        [Display(Name = "Pierwszy właściciel")]
        [Required(ErrorMessage = "Wybierz właściwą opcję")]
        public bool IsFirstOwner { get; set; }

        [Required]
        public bool IsStartStop { get; set; }

        [FromYesNo]
        [Display(Name = "Bezwypadkowy")]
        [Required(ErrorMessage = "Wybierz właściwą opcję")]
        public bool IsAccidentFree { get; set; }

        [FromYesNo]
        [Display(Name = "Angielska wersja (kierownica z prawej strony)")]
        [Required(ErrorMessage = "Wybierz właściwą opcję")]
        public bool IsEnglishVersion { get; set; }

        [Required]
        public bool IsNavi { get; set; }

        [Required]
        public bool IsTempomat { get; set; }

        [Required]
        public bool IsXenon { get; set; }

        [Required]
        public bool IsLeather { get; set; }

        [Required]
        public bool IsAutomaticClima { get; set; }

        [Required]
        public bool IsManualClima { get; set; }

        [Required]
        public bool IsSeatHeating { get; set; }

        [Required]
        public bool IsMultifunctionalSteeringWheel { get; set; }

        #endregion

        #region User Details
        public string Email { get; set; }

        [Display(Name ="Imię")]
        public string UserName { get; set; }
        public ReportType ReportType { get; set; }

 
        public string PromoCode { get; set; }
        public string PromoCodeInvalidMessage { get; set; }
        public double? PromoPriceBasic { get; set; }
        public double? PromoPricePremium { get; set; }

        #endregion


        #region Enum Collections

        public IEnumerable<string> ColorsList { get; set; } = EnumExtensions.GetEnumDescriptions<BodyColors>();
        public IEnumerable<string> FuelTypesList { get; set; } = EnumExtensions.GetEnumDescriptions<FuelTypes>();
        public IEnumerable<string> GearingTypesList { get; set; } = EnumExtensions.GetEnumDescriptions<GearingTypes>();
        public IEnumerable<string> OriginCountriesList { get; set; } = EnumExtensions.GetEnumDescriptions<OriginCountries>();
        public IEnumerable<string> YesNoList { get; set; } = EnumExtensions.GetEnumDescriptions<YesNoOption>();
        public IEnumerable<string> DoorsCountList { get; set; } = EnumExtensions.GetEnumDescriptions<DoorsCount>();

        #endregion

        #region Calculated Collections

        public IEnumerable<string> MakesList { get; set; } = new List<string>();
        public IEnumerable<string> ModelsList { get; set; } = new List<string>();
        public IEnumerable<string> VersionsList { get; set; } = new List<string>();

        public IEnumerable<string> ProductionYearsList { get; set; } = new List<string>();
        public IEnumerable<string> BodyTypesList { get; set; } = new List<string>();

        #endregion

        public ReportRequest CreateReportRequest()
        {
            return new ReportRequest
            {
                Make = Make,
                Model = Model,
                Version = Version,
                BodyType = BodyType,
                GearingType = GearingType,
                FuelType = FuelType,
                Price = 0,
                ProductionYear = ProductionYear,
                Mileage = Mileage.Value.ToString(),
                Displacement = Displacement,
                Power = Power.ToString(),
                OriginCountry = OriginCountry,
                Color = Color,
                DoorsCount = DoorsCount,

                IsBroken = IsBroken ? "Tak" : "Nie",
                IsFirstOwner = IsFirstOwner,
                IsStartStop = IsStartStop,
                IsAccidentFree = IsAccidentFree,
                IsEnglishVersion = IsEnglishVersion ? "Tak" : "Nie",

                IsNavi = IsNavi,
                IsTempomat = IsTempomat,
                IsXenon = IsXenon,
                IsLeather = IsLeather,
                IsAutomaticClima = IsAutomaticClima,
                IsManualClima = IsManualClima,
                IsSeatHeating = IsSeatHeating,
                IsMultifunctionalSteeringWheel = IsMultifunctionalSteeringWheel,

                UserName = UserName,
                Email = Email
            };
        }
    }
}
