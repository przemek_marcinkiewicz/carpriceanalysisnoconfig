﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;
using System.Reflection;

namespace CarPriceAnalysis.Web.Portal.Infrastructure.ModelBinders.FromYesNo
{
    public class FromYesNoModelProvider: IModelBinderProvider
    {
            public IModelBinder GetBinder(ModelBinderProviderContext context)
            {
                if (context == null) throw new ArgumentNullException(nameof(context));

                if (!context.Metadata.IsComplexType)
                {
                    // Look for scrubber attributes
                    var propName = context.Metadata.PropertyName;
                    if (string.IsNullOrEmpty(propName))
                    {
                        return null;
                    }
                    var propInfo = context.Metadata.ContainerType.GetProperty(propName);

                    // Only one scrubber attribute can be applied to each property
                    var attribute = propInfo.GetCustomAttributes(typeof(FromYesNoAttribute), false).FirstOrDefault();
                    if (attribute != null) return new FromYesNoModelBinder(context.Metadata.ModelType, attribute as FromYesNoAttribute);
                }

                return null;
            
        }
    }
}
