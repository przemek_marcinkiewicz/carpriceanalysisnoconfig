﻿using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Text;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class PromoCodesRepository: IPromoCodesRepository
    {
        private readonly AppDbContext _db;
        private static Random random = new Random((int)DateTime.Now.Ticks);

        public PromoCodesRepository(AppDbContext db)
        {
            _db = db;
        }

        public async Task<PromoCode> GetByCode(string code)
        {
            var promoCode = await _db.Set<PromoCode>().FirstOrDefaultAsync(p => p.Code == code);
            return promoCode;
        }

        public async Task UpdateAsync(PromoCode code)
        {
            _db.Update(code);
            await _db.SaveChangesAsync();
        }

        public async Task<PromoCode> Create(PromoCodeType type, double? priceSingle = 0, double? priceMultiple = 0, DateTime? expirationDate = default(DateTime?))
        {
            var promoCode = new PromoCode
            {
                Type = type,
                ExpirationDate = expirationDate,
                PriceMultiple = priceMultiple,
                PriceSingle = priceSingle,
                Code = $"{RandomString(3)}-{RandomString(3)}-{RandomString(3)}"
            };

            _db.Add(promoCode);
            await _db.SaveChangesAsync();
            return promoCode;
        }

        private string RandomString(int size)
        {
            Random random = new Random((int)DateTime.Now.Ticks);

            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
}