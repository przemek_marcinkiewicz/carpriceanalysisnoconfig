﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum BodyColors
    {
        [Description("Beżowy")]
        Bezowy,

        [Description("Biały")]
        Bialy,

        [Description("Bordowy")]
        Bordowy,

        [Description("Brązowy")]
        Brazowy,

        [Description("Czarny")]
        Czarny,

        [Description("Czerwony")]
        Czerwony,

        [Description("Niebieski")]
        Niebieski,

        [Description("Srebrny")]
        Srebrny,

        [Description("Szary")]
        Szary,

        [Description("Fioletowy")]
        Fioletowy,

        [Description("Zielony")]
        Zielony,

        [Description("Żółty")]
        Zolty,

        [Description("Złoty")]
        Zloty,

        [Description("inny")]
        Inny
    }
}
