﻿using CarPriceAnalysis.Common.Model.Data.Enums;
using System;

namespace CarPriceAnalysis.Common.Model.Data
{
    public class Payment
    {
        public Guid Id { get; set; }
        public string OperationNumber { get; set; }
        public PaymentStatus Status { get; set; }
        public double ExpectedAmount { get; set; }
        public double? Amount { get; set; }
        public double? Commision { get; set; }
        public double? OriginalAmount { get; set; }
        public DateTime? OperationDateTime { get; set; }
        public DateTime? ConfirmationDateTime { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public Guid ReportRequestId { get; set; }
    }
}
