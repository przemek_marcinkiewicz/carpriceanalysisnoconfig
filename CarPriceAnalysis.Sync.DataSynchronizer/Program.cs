﻿using CarPriceAnalysis.Common.Data;
using CarPriceAnalysis.Common.Data.Repositories;
using CarPriceAnalysis.Common.Model.Business;
using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Sync.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Sync.DataSynchronizer
{
    public class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Starting Data Synchronizer");

            Console.WriteLine("Creating DB Connection");
            var dbContextFactory = new AppDbContextFactory();
            var dbContext = dbContextFactory.Create();
            var queue = new QueueRepository(dbContext);
            var offerRepository = new OffersRepository(dbContext);

            Task.Run(async () =>
            {

                Console.WriteLine("Testing DB Connection");
                var recordsCount = dbContext.Set<Job>().Count();
                Console.WriteLine($"Connection OK. Number of records returned {recordsCount}");

                Console.WriteLine("Downloading next processing unit from queue.");
                var nextJob = await queue.GetNext(Common.Model.Data.Enums.JobName.GetOffers);
                Console.WriteLine($"Connection OK. Number of records returned {recordsCount}");

                if (nextJob == null)
                {
                    Console.WriteLine("No processing unit found. EXIT.");
                    return;
                }

                Console.WriteLine($"Updating processing unit - mark failed");
                await queue.UpdateStatus(nextJob, Common.Model.Data.Enums.JobResult.Fail);

                Console.WriteLine($"Next processing unit: Action={nextJob.ActionName}, Parameters={nextJob.ActionParameters}");

                var allegroApi = new AllegroApi();
                var offers = new List<AllegroCarModel>();

                //get offers
                Console.WriteLine("Initializing Allegro Web Api");
                await allegroApi.Initialize();


                Console.WriteLine("Downloading offers");

                await allegroApi.Initialize();
                var offersResponse = await allegroApi.GetOffers(2000, nextJob.ActionParameters);
                offers = offersResponse.ToList();

                //read revision number
                var revision = await offerRepository.GetRevision(long.Parse(nextJob.ActionParameters));
                Console.WriteLine($"Data revision: {revision}");

                var offerRevisions = offers.Select(x => new CarOfferDetails(x, revision)).ToList();
                await offerRepository.AddMany(offerRevisions);

                Console.WriteLine($"Updating processing unit");
                await queue.UpdateStatus(nextJob, Common.Model.Data.Enums.JobResult.Success);

            }).Wait();
        }
    }
}
