﻿namespace CarPriceAnalysis.Common.Model.Data.Enums
{
    public enum PaymentStatus
    {
        New = 1,
        Processing = 2,
        Completed = 3,
        Rejected = 4
    }
}
