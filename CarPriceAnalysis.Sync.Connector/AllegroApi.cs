﻿using AllegroWebApi;
using CarPriceAnalysis.Common.Model.Business;
using CarPriceAnalysis.Sync.Connector.Secrets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Sync.Connector
{
    public class AllegroApi
    {
        private const string WebApiKey;
        private const int CountryCode = 1;
        private static string _sessionHandle;
        private static servicePortClient _client;

        public AllegroApi()
        {
        }

        public async Task Initialize()
        {
            _client = new servicePortClient();
            var sysStatusType = await _client.doQueryAllSysStatusAsync(CountryCode, WebApiKey);

            var loginRespone = await _client.doLoginAsync(new doLoginRequest
            {
                countryCode = CountryCode,
                localVersion = sysStatusType.sysCountryStatus[0].verKey,
                userLogin = Creditentials.Login,
                userPassword = Creditentials.Password,
                webapiKey = WebApiKey
            });

            _sessionHandle = loginRespone.sessionHandlePart;
        }

        public async Task<IEnumerable<AllegroCarModel>> GetOffers(int count, string modelId)
        {
            //Get list of items filtered by FilterOptionsTypes: Category and OfferType
            //As the result itemsListType containing items Ids' is returned

            #region filters setup
            var filters = new List<FilterOptionsType>
            {
                new FilterOptionsType
                {
                    filterId = "offerType",
                    filterValueId = new[] {"buyNow"}
                },
                new FilterOptionsType
                {
                    filterId = "category",
                    filterValueId = new[] { modelId }
                }
            };

            #endregion

            var totalProcessedOffers = 0;
            var bigChunkSize = 500;
            var offset = 0;

            var itemsListChunks = new List<List<long>>();

            while (totalProcessedOffers < count)
            {
                var result = await _client.doGetItemsListAsync(new doGetItemsListRequest
                {
                    webapiKey = WebApiKey,
                    countryId = 1,
                    filterOptions = filters.ToArray(),
                    resultOffset = totalProcessedOffers,
                    resultSize = bigChunkSize,
                    resultScope = 3,
                    sortOptions = null
                });

                totalProcessedOffers += bigChunkSize;
                if (totalProcessedOffers + bigChunkSize > count)
                {
                    bigChunkSize = count - totalProcessedOffers;
                }
                offset++;

                //Get details of cars from the result list by Id
                if(result.itemsList != null)
                {

                var itemsList = result.itemsList.Select(x => x.itemId).ToList();
                //Split itemsList to 25 items chunks
                var smallChunkSize = 25;
                    for (int i = 0; i < itemsList.Count; i += smallChunkSize)
                    {
                        try
                        {
                            var chunk = itemsList.GetRange(i, smallChunkSize);
                            itemsListChunks.Add(chunk);
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                var chunk = itemsList;
                                itemsListChunks.Add(chunk);
                            }
                            catch(Exception innerEx)
                            {
                            }
                        }

                        if (i + smallChunkSize > itemsList.Count)
                        {
                            smallChunkSize = itemsList.Count - i;
                        }
                    }
                }
            }

            var carOffers = new List<AllegroCarModel>();

            foreach (var itemsListChunk in itemsListChunks)
            {
                try
                {
                    var itemsResult = await _client.doGetItemsInfoAsync(new doGetItemsInfoRequest
                    {
                        sessionHandle = _sessionHandle,
                        itemsIdArray = itemsListChunk.ToArray(),
                        getAttribs = 1,
                        getCompanyInfo = 0,
                        getDesc = 0,
                        getImageUrl = 0,
                        getPostageOptions = 0,
                        getProductInfo = 0
                    });


                    carOffers.AddRange(itemsResult.arrayItemListInfo.Select(x => new AllegroCarModel
                    {
                        OfferId = x.itemInfo.itId,
                        Color = GetDetailedValue(x.itemAttribs, "Kolor"),
                        ProductionYear = GetDetailedValue(x.itemAttribs, "Rok produkcji"),
                        BodyType = GetDetailedValue(x.itemAttribs, "Nadwozie"),
                        FuelType = GetDetailedValue(x.itemAttribs, "Rodzaj paliwa"),
                        GearingType = GetDetailedValue(x.itemAttribs, "Skrzynia biegów"),
                        OriginCountry = GetDetailedValue(x.itemAttribs, "Kraj pochodzenia"),
                        Displacement = GetDetailedValue(x.itemAttribs, "Pojemność silnika [cm3]"),
                        Mileage = GetDetailedValue(x.itemAttribs, "Przebieg [km]"),
                        DoorsCount = GetDetailedValue(x.itemAttribs, "Liczba drzwi").Replace("/", "-"), //prevent auto converting to DateTime in CSV file
                        Power = GetDetailedValue(x.itemAttribs, "Moc [KM]"),
                        IsEnglishVersion = GetDetailedValue(x.itemAttribs, "Kierownica po prawej (Anglik)"),
                        IsBroken = GetDetailedValue(x.itemAttribs, "Uszkodzony"),
                        IsAccidentFree = HasAttribInCollection(x.itemAttribs, "Bezwypadkowy", "Informacje dodatkowe"),
                        IsManualClima = HasAttribInCollection(x.itemAttribs, "Klimatyzacja automatyczna", "Wyposażenie - komfort"),
                        IsAutomaticClima = HasAttribInCollection(x.itemAttribs, "Klimatyzacja manualna", "Wyposażenie - komfort"),
                        IsSeatHeating = HasAttribInCollection(x.itemAttribs, "Podgrzewane przednie siedzenia", "Wyposażenie - komfort"),
                        IsLeather = HasAttribInCollection(x.itemAttribs, "Tapicerka skórzana", "Wyposażenie - komfort"),
                        IsMultifunctionalSteeringWheel = HasAttribInCollection(x.itemAttribs, "Wielofunkcyjna kierownica", "Wyposażenie - komfort"),
                        IsFirstOwner = HasAttribInCollection(x.itemAttribs, "Pierwszy właściciel", "Informacje dodatkowe"),
                        IsNavi = HasAttribInCollection(x.itemAttribs, "Nawigacja GPS", "Wyposażenie - multimedia"),
                        IsStartStop = HasAttribInCollection(x.itemAttribs, "System Start-Stop", "Wyposażenie - pozostałe"),
                        IsTempomat = HasAttribInCollection(x.itemAttribs, "Tempomat", "Wyposażenie - pozostałe"),
                        IsXenon = HasAttribInCollection(x.itemAttribs, "Światła xenonowe", "Wyposażenie - bezpieczeństwo"),
                        Price = x.itemInfo.itBuyNowPrice,
                        Make = x.itemCats.Length > 3 ? x.itemCats[3].catName : null,
                        Model = x.itemCats.Length > 4 ? x.itemCats[4].catName : null,
                        Version = x.itemCats.Length > 5 ? x.itemCats[5].catName : null,
                        CategoryId = x.itemCats.Length > 5 ? x.itemCats[5].catId : (x.itemCats.Length > 4 ? x.itemCats[4].catId : x.itemCats[3].catId)
                    }));
                }
                catch (Exception ex)
                {
                }
            }
            return carOffers;
        }

        public async Task<int> CountOffers(string modelId)
        {
            #region filters setup
            var filters = new List<FilterOptionsType>
            {
                new FilterOptionsType
                {
                    filterId = "offerType",
                    filterValueId = new[] {"buyNow"}
                },
                new FilterOptionsType
                {
                    filterId = "category",
                    filterValueId = new[] { modelId }
                }
            };
            #endregion

                var result = await _client.doGetItemsListAsync(new doGetItemsListRequest
                {
                    webapiKey = WebApiKey,
                    countryId = 1,
                    filterOptions = filters.ToArray(),
                    resultOffset = 0,
                    resultSize = 1,
                    resultScope = 3,
                    sortOptions = null
                });

            return result.itemsCount;
            }

        private string GetDetailedValue(AttribStruct[] itemAttribs, string attribName)
        {
            return itemAttribs.Any(y => y.attribName.Equals(attribName)) ? itemAttribs.FirstOrDefault(y => y.attribName.Equals(attribName)).attribValues[0] : null;
        }

        private bool HasAttribInCollection(AttribStruct[] itemAttribs, string attribName, string attribsCollection)
        {
            var attrib = itemAttribs.FirstOrDefault(y => y.attribName.Equals(attribsCollection));
            if (attrib != null)
                return attrib.attribValues.Any(x => x.Equals(attribName));
            return false;
        }
    }
}
