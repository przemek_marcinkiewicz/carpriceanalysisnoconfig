﻿using System;

namespace CarPriceAnalysis.Web.Portal.ViewModels
{
    public class PaymentRequestViewModel
    {
        public string id { get; set; } = "420661";
        public string amount { get; set; }
        public string currency { get; set; } = "PLN";
        public string description { get; set; }
        public string lang { get; set; } = "pl";
        public string channel_groups { get; set; } = "K,T,P";
        public string email { get; set; }
        public string url { get; set; }
        public string type { get; set; } = "3";
        public string txtguzik { get; set; } = "Kontynuuj do autocena.info";
    }
}
