﻿using System;

namespace CarPriceAnalysis.Web.Portal.ViewModels
{
    public class DownloadDetailsViewModel
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public DateTime CreateDateTime { get; set; }
        public DateTime? GenerateDateTime { get; set; }

        public string Make { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }

        public string CarImageUrl { get; set; }

    }
}
