﻿using System;

namespace CarPriceAnalysis.Web.Portal.Infrastructure.ModelBinders.FromYesNo
{
    public class FromYesNoAttribute : Attribute, IFromYesNoAttribute
    {
        public object Scrub(string modelValue, out bool success)
        {
            if (modelValue == "Tak" || modelValue == "Nie")
            {
                success = true;
                return modelValue == "Tak" ? true : false;
            }
            else
            {
                success = false;
                return null;
            }
        }
    }
}
