﻿namespace CarPriceAnalysis.Common.Model.Business
{
    public class StringTable
    {
        public string[] ColumnNames { get; set; }
        public string[,] Values { get; set; }
    }
}
