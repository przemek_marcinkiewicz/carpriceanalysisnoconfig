﻿namespace CarPriceAnalysis.Common.Model.Business
{
    public class AzureMLServiceDetails
    {
        public string Make { get; set; }
        public string ApiKey { get; set; }
        public string Url { get; set; }
    }
}
