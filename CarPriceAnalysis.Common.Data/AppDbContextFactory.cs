﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CarPriceAnalysis.Common.Data
{
    public class AppDbContextFactory : IDbContextFactory<AppDbContext>
    {
        public AppDbContext Create()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer();
            return new AppDbContext(optionsBuilder.Options);
        }

        public AppDbContext Create(DbContextFactoryOptions options)
        {
            return Create();
        }
    }
}
