﻿using System.Collections.Generic;

namespace CarPriceAnalysis.Common.Model.Business
{
    public class AzureMLResultOutputValue
    {
        public List<string> ColumnNames { get; set; }
        public List<string> ColumnTypes { get; set; }
        public List<List<string>> Values { get; set; }
    }
}
