﻿using CarPriceAnalysis.Common.Model.Business;
using System;
using System.ComponentModel.DataAnnotations;

namespace CarPriceAnalysis.Web.Portal.ViewModels
{
    public class ReportDetailsViewModel
    {
        #region Model Properties

        [Display(Name = "Marka")]
        public string Make { get; set; }

        [Display(Name = "Model")]
        public string Model { get; set; }

        [Display(Name = "Wersja")]
        public string Version { get; set; }

        [Display(Name = "Typ nadwozia")]
        public string BodyType { get; set; }

        [Display(Name = "Rodzaj skrzyni biegów")]
        public string GearingType { get; set; }

        [Display(Name = "Rodzaj paliwa")]
        public string FuelType { get; set; }

        [Display(Name = "Cena")]
        public double Price { get; set; }

        [Display(Name = "Rok produkcji")]
        public string ProductionYear { get; set; }

        [Display(Name = "Przebieg")]
        public int? Mileage { get; set; }

        [Display(Name = "Pojemność silnika")]
        public string Displacement { get; set; }

        [Display(Name = "Moc")]
        public int? Power { get; set; }

        [Display(Name = "Kraj pochodzenia")]
        public string OriginCountry { get; set; }

        [Display(Name = "Kolor")]
        public string Color { get; set; }

        [Display(Name = "Liczba drzwi")]
        public string DoorsCount { get; set; }

        [Display(Name = "Uszkodzony")]
        public bool IsBroken { get; set; }

        [Display(Name = "Pierwszy właściciel")]
        public bool IsFirstOwner { get; set; }

        [Display(Name = "System start-stop")]
        public bool IsStartStop { get; set; }

        [Display(Name = "Bezwypadkowy")]
        public bool IsAccidentFree { get; set; }

        [Display(Name = "Angielska wersja (kierownica z prawej strony)")]
        public bool IsEnglishVersion { get; set; }

        [Required]
        [Display(Name = "Nawigacja")]
        public bool IsNavi { get; set; }

        [Required]
        [Display(Name = "Tempomat")]
        public bool IsTempomat { get; set; }

        [Required]
        [Display(Name = "Reflektory ksenonowe")]
        public bool IsXenon { get; set; }

        [Required]
        [Display(Name = "Skórzana tapicerka")]
        public bool IsLeather { get; set; }

        [Required]
        [Display(Name = "Klimatyzacja automatyczna")]
        public bool IsAutomaticClima { get; set; }

        [Required]
        [Display(Name = "Klimatyzacja manualna")]
        public bool IsManualClima { get; set; }

        [Required]
        [Display(Name = "Podgrzewane fotele")]
        public bool IsSeatHeating { get; set; }

        [Required]
        [Display(Name = "Wielofukncyjna kierownica")]
        public bool IsMultifunctionalSteeringWheel { get; set; }

        #endregion

        #region User Details
        public string Email { get; set; }
        public string Phone { get; set; }
        public string UserName { get; set; }
        public string ReportType { get; set; }

        #endregion

        #region Report details
        public long CategoryId { get; set; }
        public DateTime GenerateDateTime { get; set; }
        public int NumberOfSamples { get; set; }
        public int NumberOfAllOffers { get; set; }
        public string ModelName { get; set; }
        #endregion


        public DateTime CreateDateTime { get; set; }

        public double PercentOfHigherPricesAtSameYear { get; set; }
        public double PercentOfHigherPricesAtDifferentYears { get; set; }

        #region Averages
        public double AveragePrice { get; set; }
        public double AvgPriceSameYear { get; set; }
        public double AvgPriceSameYearSimiliarMileage { get; set; }
        public double AvgPriceSameYearSimiliarDisplacement { get; set; }

        public double AvgMileage { get; set; }
        #endregion

        #region Charts Averages
        public ChartData ChartData_AvgPriceToYear { get; set; }
        public ChartData ChartData_PricesOrdered { get; set; }
        public ChartData ChartData_PricesSameYearOrdered { get; set; }
        #endregion

        #region Equipment stats
        [Display(Name = "Klimatyzacja automatyczna")]
        public double PercentOfIsAutomaticClima { get; set; }

        [Display(Name = "Skórzana tapicerka")]
        public double PercentOfIsLeather { get; set; }

        [Display(Name = "Klimatyzacja manualna")]
        public double PercentOfIsManualClima { get; set; }

        [Display(Name = "Wielofunkcyjna kierownica")]
        public double PercentOfIsMultifunctionalSteeringWheel { get; set; }

        [Display(Name = "Nawigacja")]
        public double PercentOfIsNavi { get; set; }

        [Display(Name = "Podgrzewane fotele")]
        public double PercentOfIsSeatHeating { get; set; }

        [Display(Name = "System start-stop")]
        public double PercentOfIsStartStop { get; set; }

        [Display(Name = "Tempomat")]
        public double PercentOfIsTempomat { get; set; }

        [Display(Name = "Reflektory ksenonowe")]
        public double PercentOfIsXenon { get; set; }
        #endregion

        #region Other stats
        public string OtherCarsSameYearAndSimiliarPrice { get; set; }
        public string MainPriceFactors { get; set; }
        #endregion

    }
}
