﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.proc_RemoveDuplicates
AS
BEGIN
	WITH cte AS (
	SELECT OfferId, row_number() OVER(PARTITION BY OfferId order by Revision desc) AS [rn] 
FROM dbo.CarOffers)
DELETE cte WHERE [rn] > 1
END
GO
