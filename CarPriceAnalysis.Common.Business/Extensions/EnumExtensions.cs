﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace CarPriceAnalysis.Common.Business.Extensions
{
    public static class EnumExtensions
    {
        public static string GetEnumDescription<T>(this T @enum) where T : struct
        {
            var enumValue = @enum as Enum;
            if (enumValue == null)
                throw new ArgumentException("Parameter is not an enum.");

            var field = enumValue.GetType().GetTypeInfo().GetField(enumValue.ToString());
            var attribute = field.GetCustomAttribute<DescriptionAttribute>();

            return attribute.Description;
        }

        public static IEnumerable<T> GetEnumValues<T>() where T : struct
        {
            return Enum.GetValues(typeof(T)).OfType<T>().ToList();
        }

        public static IEnumerable<string> GetEnumDescriptions<T>() where T : struct
        {
            return GetEnumValues<T>().Select(e => e.GetEnumDescription()).ToList();
        }

        public static T ToEnum<T>(this string value) where T : struct
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        public static T ToEnumFromDescription<T>(this string description) where T : struct
        {
            return GetEnumValues<T>().First(e => e.GetEnumDescription<T>() == description);
        }
    }
}