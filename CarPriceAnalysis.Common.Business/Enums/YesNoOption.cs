﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum YesNoOption
    {
        [Description("Tak")]
        Yes,

        [Description("Nie")]
        No
    }
}
