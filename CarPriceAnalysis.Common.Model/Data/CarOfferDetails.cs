﻿using CarPriceAnalysis.Common.Model.Business;
using System;

namespace CarPriceAnalysis.Common.Model.Data
{
    public class CarOfferDetails
    {
        private CarOfferDetails()
        {
        }

        public CarOfferDetails(AllegroCarModel x, int revision)
        {
            Revision = revision;
            CreateDateTime = DateTime.UtcNow;
            CategoryId = x.CategoryId;
            OfferId = x.OfferId;

            BodyType = x.BodyType;
            Color = x.Color;
            Displacement = x.Displacement;
            DoorsCount = x.DoorsCount;
            FuelType = x.FuelType;
            GearingType = x.GearingType;
            IsAccidentFree = x.IsAccidentFree;
            IsAutomaticClima = x.IsAutomaticClima;
            IsBroken = x.IsBroken;
            IsEnglishVersion = x.IsEnglishVersion;
            IsFirstOwner = x.IsFirstOwner;
            IsLeather = x.IsLeather;
            IsManualClima = x.IsManualClima;
            IsMultifunctionalSteeringWheel = x.IsMultifunctionalSteeringWheel;
            IsNavi = x.IsNavi;
            IsSeatHeating = x.IsSeatHeating;
            IsStartStop = x.IsStartStop;
            IsTempomat = x.IsTempomat;
            IsXenon = x.IsXenon;
            Make = x.Make;
            Mileage = x.Mileage;
            Model = x.Model;
            OriginCountry = x.OriginCountry;
            Power = x.Power;
            Price = x.Price;
            ProductionYear = x.ProductionYear;
            Version = x.Version;

        }

        public long Id { get; set; }
        public DateTime CreateDateTime { get; set; }
        public int Revision { get; set; }
        public long CategoryId { get; set; }

        public long OfferId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string BodyType { get; set; }

        public string GearingType { get; set; }
        public string FuelType { get; set; }
        public double Price { get; set; }
        public string ProductionYear { get; set; }
        public string Mileage { get; set; }
        public string Displacement { get; set; }
        public string Power { get; set; }
        public string OriginCountry { get; set; }
        public string Color { get; set; }
        public string DoorsCount { get; set; }

        public string IsBroken { get; set; }
        public bool IsFirstOwner { get; set; }
        public bool IsStartStop { get; set; }
        public bool IsAccidentFree { get; set; }
        public string IsEnglishVersion { get; set; }

        public bool IsNavi { get; set; }
        public bool IsTempomat { get; set; }
        public bool IsXenon { get; set; }
        public bool IsLeather { get; set; }
        public bool IsAutomaticClima { get; set; }
        public bool IsManualClima { get; set; }
        public bool IsSeatHeating { get; set; }
        public bool IsMultifunctionalSteeringWheel { get; set; }
    }
}