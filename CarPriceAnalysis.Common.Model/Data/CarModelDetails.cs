﻿using CarPriceAnalysis.Common.Model.Business;

namespace CarPriceAnalysis.Common.Model.Data
{
    public class CarModelDetails
    {
        public CarModelDetails()
        {}

        public CarModelDetails(AllegroCarModel x)
        {
            CategoryId = x.CategoryId;
            Make = x.Make;
            Model = x.Model;
            Version = x.Version;
        }

        public int Id { get; set; }
        public long CategoryId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string Types { get; set; }
        public string Years { get; set; }
        public string Details { get; set; }
        public string PhotoUrl { get; set; }
        public bool IsEnabled { get; set; }

    }
}
