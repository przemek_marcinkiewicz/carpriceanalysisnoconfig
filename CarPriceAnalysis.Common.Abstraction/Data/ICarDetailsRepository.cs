﻿using CarPriceAnalysis.Common.Model.Data;
using System.Collections.Generic;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface ICarDetailsRepository
    {
        IEnumerable<string> GetBrands();

        IEnumerable<string> GetModels(string make);

        IEnumerable<string> GetVersions(string make, string model);

        CarModelDetails Get(string make, string model, string version);

        IEnumerable<string> GetBodyTypes(string make, string model, string version);

        IEnumerable<string> GetYears(string make, string model, string version);

        IEnumerable<long> GetCategoryIds();
    }
}
