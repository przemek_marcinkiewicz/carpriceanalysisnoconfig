﻿namespace CarPriceAnalysis.Web.Portal.Infrastructure.ModelBinders.FromYesNo
{
    public interface IFromYesNoAttribute
    {
        object Scrub(string modelValue, out bool success);
    }
}
