﻿namespace CarPriceAnalysis.Common.Model.Data.Enums
{
    public enum JobResult
    {
        Waiting = 0,
        Success = 1,
        Warning = 2,
        Fail = 3
    }
}
