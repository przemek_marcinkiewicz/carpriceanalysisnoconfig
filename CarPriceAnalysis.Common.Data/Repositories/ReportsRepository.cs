﻿using CarPriceAnalysis.Common.Abstraction.Data;
using System;
using System.Threading.Tasks;
using CarPriceAnalysis.Common.Model.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class ReportsRepository : IReportsRepository
    {

        private readonly AppDbContext _db;
        public ReportsRepository(AppDbContext db)
        {
            _db = db;
        }

        public async Task<ReportRequest> FindRequestByIdAsync(Guid id)
        {
            return await _db.Set<ReportRequest>().FirstAsync(r => r.Id == id);
        }

        public async Task<ReportRequest> GetNextToProcess()
        {
            return await _db.Set<ReportRequest>().OrderBy(r=>r.CreateDateTime).FirstOrDefaultAsync(r => r.GenerateDateTime == null);
        }

        public async Task SaveRequestAsync(ReportRequest report)
        {
            _db.Add(report);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateRequestAsync(ReportRequest report)
        {
            _db.Update(report);
            await _db.SaveChangesAsync();
        }

        public async Task SaveReportAsync(ReportDetails report)
        {
            _db.Add(report);
            await _db.SaveChangesAsync();
        }

        public async Task<ReportDetails> FindReportByReportId(Guid reportId)
        {
            return await _db.Set<ReportDetails>().FirstAsync(r => r.ReportId == reportId);
        }
    }
}
