﻿namespace CarPriceAnalysis.Common.Model.Data.Enums
{
    public enum JobName
    {
        GenerateReport = 1,
        PublishReport = 2,
        SendEmail = 3,
        GetOffers = 4,
        ClearDuplicates = 5
    }
}
