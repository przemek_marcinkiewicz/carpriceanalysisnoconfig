﻿using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Common.Model.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class CarDetailsRepository: ICarDetailsRepository
    {
        private readonly AppDbContext _db;
        public CarDetailsRepository(AppDbContext db)
        {
            _db = db;
        }

        /*
          update dbo.CarModelDetails set IsEnabled = 1 where CategoryId in (
           SELECT cmd.CategoryId FROM dbo.CarModelDetails cmd where (Select count(1) from dbo.CarOffers co where co.CategoryId = cmd.CategoryId ) > 100
           )
        */
        public IEnumerable<string> GetBrands()
        {
            return _db.Set<CarModelDetails>().Where(c => c.IsEnabled).Select(x => x.Make).Distinct();
        }

        public IEnumerable<string> GetModels(string make)
        {
            return _db.Set<CarModelDetails>().Where(c => c.Make == make && c.IsEnabled).Select(x => x.Model).Distinct();
        }

        public IEnumerable<long> GetCategoryIds()
        {
            return _db.Set<CarModelDetails>().Select(x => x.CategoryId);
        }

        public IEnumerable<string> GetVersions(string make, string model)
        {
            return _db.Set<CarModelDetails>().Where(c => c.Model == model && c.Make == make && c.IsEnabled).Select(x => x.Version).Distinct();
        }

        public CarModelDetails Get(string make, string model, string version)
        {
            return _db.Set<CarModelDetails>().Where(c => c.Model == model && c.Make == make && c.Version == version).First();
        }

        public IEnumerable<string> GetBodyTypes(string make, string model, string version)
        {
            var bodyTypes = Get(make, model, version).Types;
            return string.IsNullOrEmpty(bodyTypes) ? new[] { "Hatchback", "Kabriolet", "Kombi", "Sedan/Limuzyna", "Sport/Coupe", "SUV", "Terenowy","Van (minibus)" } : bodyTypes.Split(';');
        }
        public IEnumerable<string> GetYears(string make, string model, string version)
        {
            var carYearsString = Get(make, model, version).Years;
            var startYear = 1980;
            var endYear = 2016;

            if (!string.IsNullOrEmpty(carYearsString))
            {
                var carYears = carYearsString.Split('-');
                startYear = int.Parse(carYears[0]);
                endYear = !string.IsNullOrEmpty(carYears[1]) ? int.Parse(carYears[1]) : DateTime.UtcNow.Year;
            }

            var numberOfYears = endYear - startYear + 1;
            return Enumerable.Range(startYear, numberOfYears).Select(y => y.ToString());
        }
    }
}
