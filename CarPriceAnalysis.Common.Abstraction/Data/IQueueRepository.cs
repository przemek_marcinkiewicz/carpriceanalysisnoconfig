﻿using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface IQueueRepository
    {
        Task Enqueue(JobName name, string parameters);
        Task UpdateStatus(Job job, JobResult result);
        Task<Job> GetNext(JobName actionName);
        Task EnqueueMany(JobName name, IEnumerable<string> parameters);
    }
}
