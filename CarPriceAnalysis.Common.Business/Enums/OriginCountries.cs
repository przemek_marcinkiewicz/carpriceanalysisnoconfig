﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum OriginCountries
    {

        [Description("Polska")]
        Poland,

        [Description("Niemcy")]
        Germany,

        [Description("Belgia")]
        Belgium,

        [Description("Szwajcaria")]
        Switzerland,

        [Description("Francja")]
        France,

        [Description("Holandia")]
        Netherland,

        [Description("Włochy")]
        Italy,

        [Description("Stany Zjednoczone")]
        USA,

        [Description("Wielka Brytania")]
        UK
    }
}
