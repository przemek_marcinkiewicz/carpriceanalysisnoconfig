﻿namespace CarPriceAnalysis.Common.Model.Business
{
    public class AzureMLPrediction
    {
        public bool Success { get; set; }
        public double Price { get; set; }
        public double ScoredLabelStandardDeviation { get; set; }
    }
}
