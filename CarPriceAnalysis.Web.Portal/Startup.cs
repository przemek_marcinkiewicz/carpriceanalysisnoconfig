﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using CarPriceAnalysis.Web.Portal.Configuration;
using CarPriceAnalysis.Web.Portal.Infrastructure.ModelBinders.FromYesNo;
using AutoMapper;
using CarPriceAnalysis.Web.Portal.Extensions.Filters;

namespace CarPriceAnalysis.Web.Portal
{
    public class Startup
    {
        private ILoggerFactory _loggerFactory { get; set; }
        private MapperConfiguration _mapperConfiguration { get; set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            _mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutomapperConfiguration());
            });

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSession();
            services.AddDatabase();
            services.AddMvc(config=>
            {
                config.ModelBinderProviders.Insert(0, new FromYesNoModelProvider());
                config.Filters.Add(new GlobalExceptionFilter(_loggerFactory));
            });
            services.AddServices();

            services.AddSingleton(sp => _mapperConfiguration.CreateMapper());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddFile("Logs/log-{Date}.txt");
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }

            app.UseStaticFiles();
            app.UseSession();
            app.UseMvc();
        }
    }
}
