﻿using System.Collections.Generic;

namespace CarPriceAnalysis.Common.Model.Business
{
    public class ChartData
    {
        public string Label { get; set; }
        public List<string> Values { get; set; }
        public List<string> Keys { get; set; }
    }
}
