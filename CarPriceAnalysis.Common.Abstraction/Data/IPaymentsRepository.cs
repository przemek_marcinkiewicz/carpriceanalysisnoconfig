﻿using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using System;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface IPaymentsRepository
    {
        Task AddAsync(Payment payment);

        Task<PaymentStatus> GetStatusForReportRequest(Guid reportRequestId);

        Task UpdateAsync(Payment payment);
        Task<Payment> GetByDescription(string description);
    }
}
