﻿using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Common.Model.Business;
using System.Collections.Generic;
using System.Linq;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class AzureMLServicesRepository : IAzureMLServicesRepository
    {
        public AzureMLServiceDetails GetServiceDetails(string make)
        {
            var serviceDetails = Services.FirstOrDefault(s => s.Make == make);
            if (serviceDetails == null)
            {
                serviceDetails = Services.FirstOrDefault(s => s.Make == "_default");
            }
            return serviceDetails;
        }


        private static List<AzureMLServiceDetails> Services = new List<AzureMLServiceDetails>
        {
            new AzureMLServiceDetails
            {
                Make = "_default",
                ApiKey = "uTzb9nbADcrGswwSxXuz6Pg9vrN9M2Ixw4sQDZGcZn2L8qWBIqPJGCG83vmX2FmKtGus2n68231LGcY7lunAKQ==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/6da3f0c01c4f4c93a472636ce26af2fd/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Alfa Romeo",
                ApiKey = "7pxtuDatj36VdLXA20R8WztpmQgIf41UpZI01f11HDOeE476gfPhxTTfbw/SRC80rDUsmRJyaEVPWTgjD2joAg==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/748812158e7c412fb40ca13fd1590aff/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Audi",
                ApiKey = "rGhhJTVjQOfLrpR/7RgUJ9PB6xeshUO2uN5v0WIuYe9CNiNJ0vFPsfl/zgPqAvlHtC6sUzf2K+/hS8GIxeVUvQ==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/e90280024241449c8c3c64f04c7b3d04/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "BMW",
                ApiKey = "7weK8rNaoIGoU4/5A60g8ZxUItUfVN5TC1m1w2eLR/SLRWd+GAD3FEMQq9b5b7kXU/Dn7cPN6mbeq4nGUB1R9w==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/2950d7b9c1b8475d8590d9d45a478f3d/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Citroen",
                ApiKey = "frpWAJR8BxcPYH/+b8qENMdHGvvmoAHGvdz/29eUUgZ34HBLyPylqQ/v5l3pCH+0DZzfeX3i5qDRbXVJwFQV1Q==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/a993e0ade4814f5aaa1fa722a3e79453/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Fiat",
                ApiKey ="kQ4Sh85f0T68WAcZyqyzOoDVmC6W+hrtmTkACcspejpDREylNMywemeX1eMzVr5Pqh7RStU8wg/yIZDsI2+ywA==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/01d6939ebe3c49758e6e1d2b6f478563/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Ford",
                ApiKey = "4/epfA6+lU2Y8kvDzyh7WpdrBPAHBSXWLTd/ohCI/4cUY2nuyWLtBQxM4gKeEPGb4pvhC9nUMFyJ6dhs4GaM0A==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/87260d5ee9b8483998bbfc4cb9560dfe/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Honda",
                ApiKey = "fpsYRnmzaxcJTpqdngBfZ0Js+mcuSMfehg2xCxgKPqXXWB9tRxTZlh3k4C+c1f/EVPct0wyZBo0MlMcj40zHLg==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/1c3c8e916a1f42609170aaebb905f16b/execute?api-version=2.0&details=true"
            },
            new AzureMLServiceDetails
            {
                Make = "Hyundai",
                ApiKey = "Yr1U8lmufb67qTlR44p/+dp0g1W4n6X2nH4tJBTQX+RZdh0zs5rnc4HL/VIzojCGVATeMdRi9xlc6NvaIHqiZQ==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/54454876b91b4ff5b59b1e7ac04e5cf9/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Kia",
                ApiKey = "SJngNfAOYV6vS1cltI/sM+e4qjRaEdG1lLwE7JC7rPcayz1TEdUWx02jv3fvZQ0xtA/BNzwa7InIIoTxRHsDYw==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/bc3cf37823c64518ac0a1f1e7739266f/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Mazda",
                ApiKey = "iow2OzVw3jIG9vvE0Fc4NjWA0L9wZuwSMkyrDUDrDDKOc3dkWjdYn2S0a8OsIFxjwzPwokEIpPmP2g4c50OJPg==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/2a2e1b9e1473421792bf229f97605c4e/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Mercedes-Benz",
                ApiKey = "lZT2Xw9Y5qvG0KkQ8zSbhsS5SJ8nRXQj2Zv49uF1bMPyBRYRd66O02QTbS65ah72bbA3KL9Jx7Hnd+mYtoHo0w==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/920e3cd14bff40bdaf9b4c5d02f01d43/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Nissan",
                ApiKey = "SGb4k50Dtt1IL8gWE7FROQJbJtxNTz9TBLmWJ8Ry1BoNcZKUQvKokwLZR4kL5JGcp/adAT0Y1ZB4ozj3BLcVUA==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/b93f93e2bc694814b609dfa41f5956d1/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Opel",
                ApiKey = "hk/T2pGkvSEAWVHXJQxolE8EL1f5FcQ6LeR1xDKxcSROszoD4bbr2ZiL0lVemxIUs5WzACb3HA8AWQ+vQZcZYw==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/e06774ee1d7a48738a77f100071b6f56/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Peugeot",
                ApiKey = "nZWt/LN9XvnHHkJwR8afsP8ZYle9UKRDssEoQSimeaGgRNJjDeKFxVoxM6HwNJJTRobKrRQ+ym2DMz3X1kt4jQ==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/46166e1cf2824f0e93e70b9ed1d98a53/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Porsche",
                ApiKey = "iKuTkukdypja6t6ZxH2phC4mItIQGSkc6vkjZSVNSEzqowODTpmu3PluBpbFIvblc9d5tv0ZkJSjoZOUTgETMw==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/8c21fdc8a1d848929626bfc86eb41109/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Renault",
                ApiKey = "tjLK7dY+JOSKe9mnAC3AZuZ9lPsPPHGcuADFco1pNINdnBtAIgkwUisi/KH33GVAJz5Oa+U8SfJlmUEEJU3Vaw==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/27139a8f7387466381516f5aa24b6e88/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Saab",
                ApiKey = "cPNpxhkgVzhfdsFLj34OVgqltZhDwRnZbM8XsuhU7ZOEih9/r92FeNmN0zXTz/ofUnPpso6JrGHUHX4ep0e/SQ==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/44d953cc9a374076a601fdaee0c5c6b0/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Seat",
                ApiKey = "+aj86tc9ls/j+9E3E6g6W1EyQk5x/CgvsVMh5geu+Lgca4D0U6biCs3boyVYor3lMleGfLl1efgZxFfdMfqDDA==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/155b6d7a6d16459e993de41f15ab9305/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Skoda",
                ApiKey = "dgsWI9wpphvAHBkDIPjqugfkR1e8K8TSEEQbiWIGW411n1tTsw8aBsjjU4vkwKp7uDgXCpOilfwGl0fDuCiUFw==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/3bbe9414df83433eb45d026d0f711f8b/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Suzuki",
                ApiKey = "X4BWOubriETH3QeeNG98zUHtyc5FLO6W10VChM75Zjcjd44vZ7pUHCzXgA/Oe30E/jWBOfUKkjXbaJABkrBD9A==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/5e3f4894a5d9446c994fe9dbe1c0e14d/execute?api-version=2.0&details=true"
            },
             new AzureMLServiceDetails
            {
                Make = "Toyota",
                ApiKey = "huyP0G8caK7eYophgji99VkNT6h1p0IL55SOdURWctOrIY2+6FgqmMG/9VfuFQT+qSz6vNvQSKSxHxEaEkTpdQ==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/48c3d6cb94a84d9ba4def2a14c707109/execute?api-version=2.0&details=true"
            }
             ,
             new AzureMLServiceDetails
            {
                Make = "Voklswagen",
                ApiKey = "Ym0rWKrwMossdNk7RL5O6z9p+FWn7iQoTh8TZu05C2ks1XgY/BwgX5EUp0VC6o7dDc0lEzXDn/UM2BSuBZcSiw==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/4441685e90db4b7fa3604116dbfd8039/execute?api-version=2.0&details=true"
            }
             ,
             new AzureMLServiceDetails
            {
                Make = "Volvo",
                ApiKey = "v2D8KZNKDigWCmHeRoj+cYKfl2WexpaM6qFYfMxM96A83H/KE+FGThB9UYbwA0NYmmzm70mwpJaPs3nnDMkR5Q==",
                Url = "https://ussouthcentral.services.azureml.net/workspaces/e9be819dd7d14a128bfadb9490b5e3f9/services/52999c5060c34b368446b3ac33cd7d98/execute?api-version=2.0&details=true"
            }
        };
    }
}
