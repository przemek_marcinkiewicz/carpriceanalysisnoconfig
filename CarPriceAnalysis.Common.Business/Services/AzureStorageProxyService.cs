﻿using CarPriceAnalysis.Common.Abstraction.Business;
using System.Net.Http;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Business.Services
{
    public class AzureStorageProxyService : IAzureStorageProxyService
    {
        private const string StorageUrl;
        public async Task<byte[]> DownloadBlobById(string id)
        {
            using(var httpClient = new HttpClient())
            {
                using(var request = new HttpRequestMessage(HttpMethod.Get, $"{StorageUrl}{id.ToLowerInvariant()}.pdf"))
                {
                    var response = await httpClient.SendAsync(request);
                    var result = await response.Content.ReadAsByteArrayAsync();
                    return result;
                }
            }
        }
    }
}
