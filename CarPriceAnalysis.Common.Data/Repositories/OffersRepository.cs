﻿using CarPriceAnalysis.Common.Abstraction.Data;
using System.Collections.Generic;
using System.Linq;
using CarPriceAnalysis.Common.Model.Data;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class OffersRepository : IOffersRepository
    {

        private readonly AppDbContext _db;
        public OffersRepository(AppDbContext db)
        {
            _db = db;
        }

        public IEnumerable<CarOfferDetails> GetCarsQuery(string make, string model, string version)
        {
            return _db.Set<CarOfferDetails>().Where(c => c.Make == make && c.Model == model && c.Version == version);
        }

        public async Task AddMany(IList<CarOfferDetails> offerDetails)
        {
            _db.Set<CarOfferDetails>().AddRange(offerDetails);
            await _db.SaveChangesAsync();
        }

        public async Task<int> GetRevision(long categoryId)
        {
            var existingModels = _db.Set<CarOfferDetails>().Where(x => x.CategoryId == categoryId);
            if (existingModels.Any())
            {
                return await existingModels.MaxAsync(m => m.Revision);
            }
            return 0;
        }

        public async Task<int> GetAllOffersCount()
        {
            var offersCount = await _db.Set<CarOfferDetails>().CountAsync();
            return offersCount;
        }
    }
}
