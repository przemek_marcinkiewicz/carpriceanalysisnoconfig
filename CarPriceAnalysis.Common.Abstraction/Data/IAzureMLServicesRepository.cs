﻿using CarPriceAnalysis.Common.Model.Business;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface IAzureMLServicesRepository
    {
        AzureMLServiceDetails GetServiceDetails(string make);
    }
}
