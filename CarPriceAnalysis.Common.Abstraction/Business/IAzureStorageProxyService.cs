﻿using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Business
{
    public interface IAzureStorageProxyService
    {
        Task<byte[]> DownloadBlobById(string id);
    }
}
