﻿using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class PaymentsRepository: IPaymentsRepository
    {
        private readonly AppDbContext _db;
        public PaymentsRepository(AppDbContext db)
        {
            _db = db;
        }

        public async Task<Payment> GetByDescription(string description)
        {
            var payment = await _db.Set<Payment>().FirstOrDefaultAsync(p => p.Description == description);
            return payment;
        }

        public async Task AddAsync(Payment payment)
        {
            _db.Add(payment);
            await _db.SaveChangesAsync();
        }

        public async Task<PaymentStatus> GetStatusForReportRequest(Guid reportRequestId)
        {
            var payment = await _db.Set<Payment>().FirstAsync(p => p.ReportRequestId == reportRequestId);
            return payment.Status;
        }

        public async Task UpdateAsync(Payment payment)
        {
            _db.Update(payment);
            await _db.SaveChangesAsync();
        }
    }
}
