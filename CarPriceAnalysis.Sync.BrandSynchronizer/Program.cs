﻿using CarPriceAnalysis.Common.Data;
using CarPriceAnalysis.Common.Model.Business;
using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Sync.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Sync.BrandSynchronizer
{
    public class Program
    {
        public const int offersCount = 2500;
        public const string categoryId = "4029";

        public static void Main(string[] args)
        {
            Console.WriteLine("Starting Brand Synchronizer");
            var allegroApi = new AllegroApi();
            var offers = new List<AllegroCarModel>();

            Console.WriteLine("Creating DB Connection");
            var dbContextFactory = new AppDbContextFactory();
            var dbContext = dbContextFactory.Create();

            Console.WriteLine("Testing DB Connection");
            var recordsCount = dbContext.Set<CarModelDetails>().Count();
            Console.WriteLine($"Connection OK. Number of records returned {recordsCount}");

            Console.WriteLine("Initializing Allegro WebAPI");
            //Initialize allegroApi
            Task.Run(async () =>{
                await allegroApi.Initialize();
            }).Wait();


            Console.WriteLine("Downloading offers from Allegro WebAPI");
            //get offers
            Task.Run(async () =>{
                var offersResponse = await allegroApi.GetOffers(offersCount, categoryId);
                offers = offersResponse.ToList();
            }).Wait();

            Console.WriteLine($"Succesfully Downloaded {offers.Count} offers from Allegro WebAPI");

            //get and save makes/models
            var carDetails = offers.Select(x => new CarModelDetails(x)).ToList();
            carDetails = carDetails.GroupBy(x => x.CategoryId).Select(x => x.First()).ToList();
            Console.WriteLine($"Downloaded {carDetails.Count} distinct models");

            var existingCarDetailsIds = dbContext.Set<CarModelDetails>().Select(x => x.CategoryId);
            Console.WriteLine($"Downloaded {existingCarDetailsIds.Count()} existing models Ids");

            var carsDetailsToCheckCount = carDetails.Where(c => !existingCarDetailsIds.Contains(c.CategoryId));
            Console.WriteLine($"Filtered downloaded models with existing. {carsDetailsToCheckCount.Count()} left.");

            var carDetailsToSave = new List<CarModelDetails>();

            Console.WriteLine($"Filtering models (required more than 50 offers).");
            foreach (var newCarDetails in carsDetailsToCheckCount)
            {
                var offersCount = 0;
                Task.Run(async () =>{
                    offersCount = await allegroApi.CountOffers(newCarDetails.CategoryId.ToString());
                }).Wait();
                if(offersCount > 50)
                {
                    carDetailsToSave.Add(newCarDetails);
                }
            }
            Console.WriteLine($"SUCCESS: Filtered models to save: {carDetailsToSave.Count()}.");
            dbContext.Set<CarModelDetails>().AddRange(carDetailsToSave);
            dbContext.SaveChanges();
            Console.WriteLine($"Models saved. EXIT.");
        }
    }
}
