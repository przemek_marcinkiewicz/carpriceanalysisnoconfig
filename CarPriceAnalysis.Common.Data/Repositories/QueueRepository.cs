﻿using CarPriceAnalysis.Common.Abstraction.Data;
using System.Threading.Tasks;
using CarPriceAnalysis.Common.Model.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using CarPriceAnalysis.Common.Model.Data.Enums;
using System;
using System.Collections.Generic;

namespace CarPriceAnalysis.Common.Data.Repositories
{
    public class QueueRepository : IQueueRepository
    {
        private readonly AppDbContext _db;
        public QueueRepository(AppDbContext db)
        {
            _db = db;
        }
        public async Task Enqueue(JobName name, string parameters)
        {
            var job = new Job
            {
                ActionName = name,
                ActionParameters = parameters,
                EnqueueDate = DateTime.UtcNow,
                ProcessingResult = JobResult.Waiting
            };

            _db.Set<Job>().Add(job);
            await _db.SaveChangesAsync();
        }

        public async Task EnqueueMany(JobName name, IEnumerable<string> parameters)
        {
            var jobList = new List<Job>();
            foreach (var parameter in parameters)
            {
                var job = new Job
                {
                    ActionName = name,
                    ActionParameters = parameter,
                    EnqueueDate = DateTime.UtcNow,
                    ProcessingResult = JobResult.Waiting
                };
                jobList.Add(job);
            }
            _db.Set<Job>().AddRange(jobList);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateStatus(Job job, JobResult result)
        {
            job.ProcessingDate = DateTime.UtcNow;
            job.ProcessingResult = result;
            await _db.SaveChangesAsync();
        }

        public async Task<Job> GetNext(JobName actionName)
        {
            var x = await _db.Set<Job>().ToListAsync();
            return await _db.Set<Job>().OrderByDescending(u => u.EnqueueDate).FirstOrDefaultAsync(u => u.ActionName == actionName && u.ProcessingDate == null);
        }
    }
}
