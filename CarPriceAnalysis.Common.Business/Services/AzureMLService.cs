﻿using CarPriceAnalysis.Common.Abstraction.Business;
using CarPriceAnalysis.Common.Model.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Business.Services
{
    public class AzureMLService : IAzureMLService
    {
        private string _apiKey;
        private string _url;

        public AzureMLService()
        {
        }

        public AzureMLService(AzureMLServiceDetails service)
        {
            _apiKey = service.ApiKey;
            _url = service.Url;
        }

        public async Task<AzureMLPrediction> GetPricePrediction(AzureMLRequest dataModel)
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {
                    Inputs = new Dictionary<string, StringTable> {
                        {
                            "input1",
                            new StringTable()
                            {
                                ColumnNames = new[]
                                {
                                    "OfferId",
                                    "Make",
                                    "Model",
                                    "Version",
                                    "BodyType",
                                    "GearingType",
                                    "FuelType",
                                    "Price",
                                    "ProductionYear",
                                    "Mileage",
                                    "Displacement",
                                    "Power",
                                    "OriginCountry",
                                    "Color",
                                    "DoorsCount",
                                    "IsBroken",
                                    "IsFirstOwner",
                                    "IsStartStop",
                                    "IsAccidentFree",
                                    "IsEnglishVersion",
                                    "IsNavi",
                                    "IsTempomat",
                                    "IsXenon",
                                    "IsLeather",
                                    "IsAutomaticClima",
                                    "IsManualClima",
                                    "IsSeatHeating",
                                    "IsMultifunctionalSteeringWheel",
                                    "Revision",
                                    "CategoryId",
                                    "Id",
                                    "CreateDateTime"
                                },
                                Values = new[,] {
                                    {
                                        dataModel.OfferId.ToString(),
                                        dataModel.Make,
                                        dataModel.Model,
                                        dataModel.Version,
                                        dataModel.BodyType,
                                        dataModel.GearingType,
                                        dataModel.FuelType,
                                        dataModel.Price.ToString("#,##"),
                                        dataModel.ProductionYear,
                                        dataModel.Mileage,
                                        dataModel.Displacement,
                                        dataModel.Power,
                                        dataModel.OriginCountry,
                                        dataModel.Color,
                                        dataModel.DoorsCount,
                                        dataModel.IsBroken.ToUpper() == "TAK" || dataModel.IsBroken.ToUpper() =="TRUE" ? "Tak": "Nie",
                                        dataModel.IsFirstOwner ? "1": "0",
                                        dataModel.IsStartStop ? "1": "0",
                                        dataModel.IsAccidentFree ? "1": "0",
                                        dataModel.IsEnglishVersion.ToUpper() == "TAK" || dataModel.IsEnglishVersion.ToUpper() =="TRUE" ? "Tak": "Nie",
                                        dataModel.IsNavi ? "1": "0",
                                        dataModel.IsTempomat ? "1": "0",
                                        dataModel.IsXenon ? "1": "0",
                                        dataModel.IsLeather ? "1": "0",
                                        dataModel.IsAutomaticClima ? "1": "0",
                                        dataModel.IsManualClima ? "1": "0",
                                        dataModel.IsSeatHeating ? "1": "0",
                                        dataModel.IsMultifunctionalSteeringWheel ? "1": "0",
                                        "0",
                                        "0",
                                        "0",
                                        "2016-11-10T11:35:23.1153807"
                                    }
                                }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _apiKey);

                client.BaseAddress = new Uri(_url);

                HttpResponseMessage response = await client.PostAsync("", new StringContent(JsonConvert.SerializeObject(scoreRequest), Encoding.UTF8, "application/json"));

                if (response.IsSuccessStatusCode)
                {
                    string resultJSON = await response.Content.ReadAsStringAsync();
                    var azureMLResult = JsonConvert.DeserializeObject<AzureMLResult>(resultJSON);

                    var columnNames = azureMLResult.Results.Output1.Value.ColumnNames;
                    var columnValues = azureMLResult.Results.Output1.Value.Values[0];

                    return new AzureMLPrediction
                    {
                        Price = double.Parse(columnValues[columnNames.FindIndex(x => x.Equals("Scored Label Mean"))], CultureInfo.InvariantCulture),
                        ScoredLabelStandardDeviation = double.Parse(columnValues[columnNames.FindIndex(x => x.Equals("Scored Label Standard Deviation"))], CultureInfo.InvariantCulture),
                        Success = true
                    };
                }

                string responseContent = await response.Content.ReadAsStringAsync();
                return new AzureMLPrediction
                {
                    Success = false
                };
            }
        }

        public void SetAzureMLServiceDetails(AzureMLServiceDetails service)
        {
            _apiKey = service.ApiKey;
            _url = service.Url;
        }
    }
}
