﻿using CarPriceAnalysis.Common.Model.Business;
using System.Collections.Generic;
using System.Linq;

namespace CarPriceAnalysis.Common.Business.Extensions
{
    public static class StringToChartExtensions
    {
        public static ChartData ToChartData (this string stringValue)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return null;
            }

            var keys = new List<string>();
            var values = new List<string>();

            var keyValues = stringValue.Split(';');
            foreach (var kv in keyValues)
            {
                var pair = kv.Split(':');
                if (pair.Length == 2)
                {
                    keys.Add(pair[0]);
                    values.Add(pair[1]);
                }
                else if (pair.Length == 1)
                {
                    values.Add(pair[0]);
                }
            }
            var chartData = new ChartData
            {
                Keys = keys,
                Values = values
            };

            return chartData;
        }

        public static ChartData ToLinearChartData(this string stringValue, int scaleStep)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return null;
            }

            var dictionary = new Dictionary<string, string>();
            var keyValues = stringValue.Split(';');
            foreach (var kv in keyValues)
            {
                var pair = kv.Split(':');
                if (pair.Length == 2)
                {
                    dictionary[pair[0]] = pair[1];
                }
            }
            var minKey = dictionary.Keys.Min(x => double.Parse(x, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo));
            var maxKey = dictionary.Keys.Max(x => double.Parse(x, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo));


            var keys = new List<string>();
            var values = new List<string>();

            for (var i = minKey; i< maxKey+scaleStep; i+= scaleStep)
            {
                var key = i.ToString("0");
                var val = string.Empty;

                keys.Add(key);
                if(dictionary.TryGetValue(key, out val))
                {
                    values.Add(val);
                }else
                {
                    values.Add("0");
                }
            }
            var chartData = new ChartData
            {
                Keys = keys,
                Values = values
            };

            return chartData;
        }
    }
}
