﻿using System;

namespace CarPriceAnalysis.Common.Model.Data
{
    public class ReportDetails
    {
        public Guid Id { get; set; }
        public Guid ReportId { get; set; }
        public DateTime CreateDateTime { get; set; }

        public double Price { get; set; }
        public double PercentOfHigherPricesAtSameYear { get; set; }
        public double PercentOfHigherPricesAtDifferentYears { get; set; }

        #region Averages
        public double AveragePrice { get; set; }
        public double AvgPriceSameYear { get; set; }
        public double AvgPriceSameYearSimiliarMileage { get; set; }
        public double AvgPriceSameYearSimiliarDisplacement { get; set; }

        public double AvgMileage { get; set; }
        #endregion

        #region Charts Averages
        public string ChartAvgPriceToYear { get; set; }
        public string ChartPricesOrdered { get; set; }
        public string ChartPricesSameYearOrdered { get; set; }
        #endregion

        #region Equipment stats
        public double PercentOfIsAutomaticClima { get; set; }
        public double PercentOfIsLeather { get; set; }
        public double PercentOfIsManualClima { get; set; }
        public double PercentOfIsMultifunctionalSteeringWheel { get; set; }
        public double PercentOfIsNavi { get; set; }
        public double PercentOfIsSeatHeating { get; set; }
        public double PercentOfIsStartStop { get; set; }
        public double PercentOfIsTempomat { get; set; }
        public double PercentOfIsXenon { get; set; }
        #endregion

        #region Other stats
        public string OtherCarsSameYearAndSimiliarPrice { get; set; }
        public string MainPriceFactors { get; set; }
        public int NumberOfSamples { get; set; }
        public int NumberOfAllOffers { get; set; }
        #endregion

    }
}
