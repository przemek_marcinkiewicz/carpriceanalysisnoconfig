﻿using CarPriceAnalysis.Common.Model.Data.Enums;
using System;

namespace CarPriceAnalysis.Common.Model.Data
{
    public class PromoCode
    {
        public Guid Id { get; set; }
        public PromoCodeType Type { get; set; }
        public string Code { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public double? PriceSingle { get; set; }
        public double? PriceMultiple { get; set; }
        public Guid? ReportRequestId { get; set; }
    }
}
