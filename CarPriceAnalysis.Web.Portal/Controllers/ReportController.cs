﻿using Microsoft.AspNetCore.Mvc;
using CarPriceAnalysis.Web.Portal.ViewModels;
using CarPriceAnalysis.Common.Abstraction.Data;
using CarPriceAnalysis.Common.Abstraction.Business;
using System.Threading.Tasks;
using AutoMapper;
using CarPriceAnalysis.Common.Model.Data;
using System;
using CarPriceAnalysis.Common.Business.Enums;
using CarPriceAnalysis.Common.Business.Extensions;
using CarPriceAnalysis.Common.Model.Data.Enums;
using System.Globalization;
using Microsoft.Extensions.Logging;

namespace CarPriceAnalysis.Web.Portal.Controllers
{
    [Route("Raport")]
    public class ReportController : Controller
    {
        private ICarDetailsRepository _repository;
        private IAzureMLService _azureMLService;
        private IAzureStorageProxyService _storageService;
        private IReportsRepository _reportsRepository;
        private IPaymentsRepository _paymentRepository;
        private IQueueRepository _queue;
        private IPromoCodesRepository _promoCodesRepository;
        private IMapper _mapper;
        private ILogger<ReportController> _logger;

        public ReportController(IAzureMLService azureMLService, IAzureStorageProxyService storageService, ICarDetailsRepository repository, IQueueRepository queue, IPaymentsRepository paymentRepository, IReportsRepository reportsRepository, IPromoCodesRepository promoCodesRepository, IMapper mapper, ILogger<ReportController> logger)
        {
            _repository = repository;
            _azureMLService = azureMLService;
            _storageService = storageService;
            _reportsRepository = reportsRepository;
            _paymentRepository = paymentRepository;
            _promoCodesRepository = promoCodesRepository;
            _queue = queue;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("")]
        public IActionResult Index(string make, string model, string version, string promoCode)
        {
            var carDetails = new CarDetailsViewModel()
            {
                MakesList = _repository.GetBrands(),
                Make = make,
                Model = model,
                Version = version,
                PromoCode = promoCode
            };
            return View(carDetails);
        }

        [HttpPost("")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(CarDetailsViewModel viewModel)
        {
            _logger.LogInformation($"{nameof(Index)} POST Car {{{viewModel.Make} {viewModel.Model} {viewModel.Version}}}");
            if (!ModelState.IsValid)
            {
                _logger.LogWarning($"{nameof(Index)} Invalid ModelState.");
                viewModel.MakesList = _repository.GetBrands();
                viewModel.ModelsList = _repository.GetModels(viewModel.Make);
                viewModel.VersionsList = _repository.GetVersions(viewModel.Make, viewModel.Model);
                viewModel.BodyTypesList = _repository.GetBodyTypes(viewModel.Make, viewModel.Model, viewModel.Version);
                viewModel.ProductionYearsList = _repository.GetYears(viewModel.Make, viewModel.Model, viewModel.Version);
                TempData["ShowNextStep"] = true;
                return View(nameof(Index), viewModel);
            }
            _logger.LogWarning($"{nameof(Index)} Redirecting to summary.");
            return RedirectToAction(nameof(Summary), viewModel);
        }

        [HttpGet("Podsumowanie")]
        public async Task<IActionResult> Summary(CarDetailsViewModel carDetails)
        {
            if (!string.IsNullOrEmpty(carDetails.PromoCode))
            {
                var promoCode = await _promoCodesRepository.GetByCode(carDetails.PromoCode);
                if (promoCode == null)
                {
                    carDetails.PromoCodeInvalidMessage = "Niepoprawny kod promocyjny";
                    carDetails.PromoCode = null;
                }
                else
                {
                    if (promoCode.ExpirationDate.HasValue && promoCode.ExpirationDate > DateTime.UtcNow)
                    {
                        carDetails.PromoCodeInvalidMessage = "Kod promocyjny stracił ważność";
                        carDetails.PromoCode = null;
                    }
                    else if (promoCode.ReportRequestId.HasValue)
                    {
                        carDetails.PromoCodeInvalidMessage = "Kod promocyjny został już wykorzystany";
                        carDetails.PromoCode = null;
                    }
                    else
                    {
                        carDetails.PromoPriceBasic = promoCode.PriceSingle;
                        carDetails.PromoPricePremium = promoCode.PriceMultiple;
                    }
                }
            }
            return View(carDetails);
        }

        [HttpPost("Zamowienie")]
        public async Task<IActionResult> Order(CarDetailsViewModel carDetails)
        {
            PromoCode promoCode = null;
            if (string.IsNullOrEmpty(carDetails.UserName))
            {
                ModelState.AddModelError(nameof(carDetails.UserName), "Imię i nazwisko jest wymagane");
            }
            if (string.IsNullOrEmpty(carDetails.Email))
            {
                ModelState.AddModelError(nameof(carDetails.Email), "Poprawny email jest wymagany w celu płatności i wysyłki raportu");
            }
            if (!string.IsNullOrEmpty(carDetails.PromoCode))
            {
                promoCode = await _promoCodesRepository.GetByCode(carDetails.PromoCode);
            }
            if (!ModelState.IsValid)
            {
                return View(nameof(Summary), carDetails);
            }

            var report = _mapper.Map<CarDetailsViewModel, ReportRequest>(carDetails);
            await _reportsRepository.SaveRequestAsync(report);

            var paymentAmount = carDetails.ReportType == ReportType.Basic ? PaymentAmount.Basic.GetEnumDescription() : PaymentAmount.Premium.GetEnumDescription();

            if (promoCode != null)
            {
                if (promoCode.Type != PromoCodeType.Multiple)
                {
                    promoCode.ReportRequestId = report.Id;
                    await _promoCodesRepository.UpdateAsync(promoCode);
                }

                var paymentAmountDouble = carDetails.ReportType == ReportType.Basic ? promoCode.PriceSingle : promoCode.PriceMultiple;
                if (!paymentAmountDouble.HasValue || paymentAmountDouble <= 0)
                {
                    await _queue.Enqueue(JobName.GenerateReport, report.Id.ToString());
                    return RedirectToAction(nameof(Confirmation));
                }
                paymentAmount = paymentAmountDouble.Value.ToString("0.00").Replace(',', '.');
            }

            var paymentViewModel = new PaymentRequestViewModel()
            {
                amount = paymentAmount,
                description = $"Raport {DateTime.UtcNow.Ticks}",
                email = carDetails.Email,
                url = "http://autocena.info/Raport/Potwierdzenie"
            };

            var payment = new Payment()
            {
                ExpectedAmount = double.Parse(paymentViewModel.amount, CultureInfo.InvariantCulture),
                CreateDateTime = DateTime.UtcNow,
                Description = paymentViewModel.description,
                ReportRequestId = report.Id,
                Status = PaymentStatus.New
            };
            await _paymentRepository.AddAsync(payment);

            return View(paymentViewModel);
        }

        [HttpGet("Potwierdzenie")]
        public IActionResult Confirmation()
        {
            return View();
        }

        [HttpPost("Potwierdzenie")]
        public IActionResult PostConfirmation(DotpayConfirmationViewModel viewModel)
        {
            return View(nameof(Confirmation));
        }

        [HttpGet("Sukces/{id}")]
        public IActionResult Success(string id)
        {
            return View(nameof(Success), id);
        }

        [HttpGet("/pdf/{id}")]
        public async Task<IActionResult> PdfPreview(string id)
        {
            var report = await _reportsRepository.FindRequestByIdAsync(new Guid(id));
            var reportResults = await _reportsRepository.FindReportByReportId(report.Id);
            var viewModel = _mapper.Map<ReportRequest, ReportDetailsViewModel>(report);
            _mapper.Map<ReportDetails, ReportDetailsViewModel>(reportResults, viewModel);

            var carModel = _repository.Get(report.Make, report.Model, report.Version);
            viewModel.CategoryId = carModel.CategoryId;

            return View(viewModel);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> DownloadPreview(string id)
        {
            _logger.LogInformation($"DOWNLOAD FILE: {id}");
            var report = await _reportsRepository.FindRequestByIdAsync(new Guid(id));
            var viewModel = _mapper.Map<ReportRequest, DownloadDetailsViewModel>(report);

            return View(nameof(DownloadPreview), viewModel);
        }

        [HttpGet("Pobierz/{id}")]
        public async Task<IActionResult> Download(string id, string accessCode)
        {
            var fileBytes = await _storageService.DownloadBlobById(id);
            return File(fileBytes, "application/pdf", "raport.pdf");
        }

        #region AJAX Methods

        [HttpGet("GetCarModels/{make}")]
        public JsonResult GetCarModels(string make)
        {
            return Json(_repository.GetModels(make));
        }

        [HttpGet("GetCarVersions/{make}/{model}")]
        public JsonResult GetCarVersions(string make, string model)
        {
            return Json(_repository.GetVersions(make, model));
        }

        [HttpGet("GetCarBodyTypes/{make}/{model}/{version?}")]
        public JsonResult GetCarBodyTypes(string make, string model, string version)
        {
            return Json(_repository.GetBodyTypes(make, model, version));
        }

        [HttpGet("GetCarProductionYears/{make}/{model}/{version?}")]
        public JsonResult GetCarProductionYears(string make, string model, string version)
        {
            return Json(_repository.GetYears(make, model, version));
        }
        #endregion
    }
}
