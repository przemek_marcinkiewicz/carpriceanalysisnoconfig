﻿namespace CarPriceAnalysis.Common.Model.Business
{
    public class AzureMLResultOutput
    {
        public string Type { get; set; }
        public AzureMLResultOutputValue Value { get; set; }
    }
}