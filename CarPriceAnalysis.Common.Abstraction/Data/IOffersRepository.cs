﻿using CarPriceAnalysis.Common.Model.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Common.Abstraction.Data
{
    public interface IOffersRepository
    {
        IEnumerable<CarOfferDetails> GetCarsQuery(string make, string model, string version);
        Task AddMany(IList<CarOfferDetails> offerDetails);
        Task<int> GetRevision(long categoryId);
        Task<int> GetAllOffersCount();
    }
}
