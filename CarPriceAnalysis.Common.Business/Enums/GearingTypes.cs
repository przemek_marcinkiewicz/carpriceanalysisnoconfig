﻿using System.ComponentModel;

namespace CarPriceAnalysis.Common.Business.Enums
{
    public enum GearingTypes
    {
        [Description("Automatyczna hydrauliczna (klasyczna)")]
        AutomatClassic,

        [Description("Automatyczna bezstopniowa CVT")]
        AutomatCVT,
        [Description("Automatyczna dwusprzęgłowa (DCT, DSG)")]
        AutomatDSG,

        [Description("Półautomatyczna (ASG)")]
        HalfAutomatASG,

        [Description("Manualna")]
        Manual,
    }
}
