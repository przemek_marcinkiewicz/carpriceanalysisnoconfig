﻿using CarPriceAnalysis.Common.Data;
using CarPriceAnalysis.Common.Data.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CarPriceAnalysis.Sync.Scheduler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Starting Scheduler");

            Console.WriteLine("Creating DB Connection");
            var dbContextFactory = new AppDbContextFactory();
            var dbContext = dbContextFactory.Create();
            var queue = new QueueRepository(dbContext);
            var carDetailsRepository = new CarDetailsRepository(dbContext);
            Task.Run(async () =>
            {
                Console.WriteLine($"Downloading car model Ids");
                var existingCarDetailsIds = carDetailsRepository.GetCategoryIds();

                Console.WriteLine($"Adding {existingCarDetailsIds.Count()} jobs");
                await queue.EnqueueMany(Common.Model.Data.Enums.JobName.GetOffers, existingCarDetailsIds.Select(id => id.ToString()));

                Console.WriteLine("Enqueue Duplicates Cleaning");
                await queue.Enqueue(Common.Model.Data.Enums.JobName.ClearDuplicates, null);
               
                Console.WriteLine($"Success EXIT.");
            }).Wait();
        }
    }
}
