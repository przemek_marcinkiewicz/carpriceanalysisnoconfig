﻿using CarPriceAnalysis.Common.Model.Data;

namespace CarPriceAnalysis.Common.Model.Business
{
    public class AzureMLRequest
    {
        public long OfferId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public string BodyType { get; set; }

        public string GearingType { get; set; }
        public string FuelType { get; set; }
        public double Price { get; set; }
        public string ProductionYear { get; set; }
        public string Mileage { get; set; }
        public string Displacement { get; set; }
        public string Power { get; set; }
        public string OriginCountry { get; set; }
        public string Color { get; set; }
        public string DoorsCount { get; set; }

        public string IsBroken { get; set; }
        public bool IsFirstOwner { get; set; }
        public bool IsStartStop { get; set; }
        public bool IsAccidentFree { get; set; }
        public string IsEnglishVersion { get; set; }

        public bool IsNavi { get; set; }
        public bool IsTempomat { get; set; }
        public bool IsXenon { get; set; }
        public bool IsLeather { get; set; }
        public bool IsAutomaticClima { get; set; }
        public bool IsManualClima { get; set; }
        public bool IsSeatHeating { get; set; }
        public bool IsMultifunctionalSteeringWheel { get; set; }
    }
}
