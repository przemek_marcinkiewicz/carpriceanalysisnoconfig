﻿using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Common.Model.Data.Enums;
using Microsoft.EntityFrameworkCore;

namespace CarPriceAnalysis.Common.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureModelMappgin(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private void ConfigureModelMappgin(ModelBuilder modelBuilder)
        {
            var carModelDetails = modelBuilder.Entity<CarModelDetails>();
            carModelDetails.Property(c => c.Id);
            carModelDetails.Property(c => c.CategoryId);
            carModelDetails.Property(c => c.Make);
            carModelDetails.Property(c => c.Model);
            carModelDetails.Property(c => c.Years).IsRequired(false);
            carModelDetails.Property(c => c.Version).IsRequired(false);
            carModelDetails.Property(c => c.Types).IsRequired(false);
            carModelDetails.Property(c => c.Details).IsRequired(false);
            carModelDetails.Property(c => c.PhotoUrl).IsRequired(false);
            carModelDetails.Property(c => c.IsEnabled);

            carModelDetails.HasKey(c => c.Id);
            carModelDetails.ToTable("CarModelDetails");

            var carMiningModel = modelBuilder.Entity<CarOfferDetails>();
            carMiningModel.Property(c => c.Id);
            carMiningModel.Property(c => c.OfferId);
            carMiningModel.Property(c => c.BodyType);
            carMiningModel.Property(c => c.Color);
            carMiningModel.Property(c => c.CreateDateTime);
            carMiningModel.Property(c => c.Displacement);
            carMiningModel.Property(c => c.DoorsCount);
            carMiningModel.Property(c => c.FuelType);
            carMiningModel.Property(c => c.GearingType);
            carMiningModel.Property(c => c.IsAccidentFree);
            carMiningModel.Property(c => c.IsAutomaticClima);
            carMiningModel.Property(c => c.IsAutomaticClima);
            carMiningModel.Property(c => c.IsBroken);
            carMiningModel.Property(c => c.IsEnglishVersion);
            carMiningModel.Property(c => c.IsFirstOwner);
            carMiningModel.Property(c => c.IsLeather);
            carMiningModel.Property(c => c.IsManualClima);
            carMiningModel.Property(c => c.IsMultifunctionalSteeringWheel);
            carMiningModel.Property(c => c.IsNavi);
            carMiningModel.Property(c => c.IsSeatHeating);
            carMiningModel.Property(c => c.IsStartStop);
            carMiningModel.Property(c => c.IsTempomat);
            carMiningModel.Property(c => c.IsXenon);
            carMiningModel.Property(c => c.Make);
            carMiningModel.Property(c => c.Mileage);
            carMiningModel.Property(c => c.Model);
            carMiningModel.Property(c => c.OfferId);
            carMiningModel.Property(c => c.OriginCountry);
            carMiningModel.Property(c => c.Power);
            carMiningModel.Property(c => c.Price);
            carMiningModel.Property(c => c.ProductionYear);
            carMiningModel.Property(c => c.Revision);
            carMiningModel.Property(c => c.Version);

            carMiningModel.HasKey(c => c.Id);
            carMiningModel.ToTable("CarOffers");

            var reportRequestDetails = modelBuilder.Entity<ReportRequest>();
            reportRequestDetails.Property(c => c.Id);
            reportRequestDetails.Property(c => c.BodyType);
            reportRequestDetails.Property(c => c.Displacement);
            reportRequestDetails.Property(c => c.DoorsCount);
            reportRequestDetails.Property(c => c.FuelType);
            reportRequestDetails.Property(c => c.GearingType);
            reportRequestDetails.Property(c => c.IsAccidentFree);
            reportRequestDetails.Property(c => c.IsAutomaticClima);
            reportRequestDetails.Property(c => c.IsAutomaticClima);
            reportRequestDetails.Property(c => c.IsBroken);
            reportRequestDetails.Property(c => c.IsEnglishVersion);
            reportRequestDetails.Property(c => c.IsFirstOwner);
            reportRequestDetails.Property(c => c.IsLeather);
            reportRequestDetails.Property(c => c.IsManualClima);
            reportRequestDetails.Property(c => c.IsMultifunctionalSteeringWheel);
            reportRequestDetails.Property(c => c.IsNavi);
            reportRequestDetails.Property(c => c.IsSeatHeating);
            reportRequestDetails.Property(c => c.IsStartStop);
            reportRequestDetails.Property(c => c.IsTempomat);
            reportRequestDetails.Property(c => c.IsXenon);
            reportRequestDetails.Property(c => c.Make);
            reportRequestDetails.Property(c => c.Mileage);
            reportRequestDetails.Property(c => c.Model);
            reportRequestDetails.Property(c => c.OriginCountry);
            reportRequestDetails.Property(c => c.Power);
            reportRequestDetails.Property(c => c.Price);
            reportRequestDetails.Property(c => c.ProductionYear);
            reportRequestDetails.Property(c => c.Version);
            reportRequestDetails.Property(c => c.FileUrl).IsRequired(false);
            reportRequestDetails.Property(c => c.CreateDateTime);
            reportRequestDetails.Property(c => c.Email);
            reportRequestDetails.Property(c => c.UserName);
            reportRequestDetails.Property(c => c.GenerateDateTime).IsRequired(false);
            reportRequestDetails.Property(c => c.Phone);

            reportRequestDetails.HasKey(c => c.Id);
            reportRequestDetails.ToTable("ReportRequests");



            var jobModel = modelBuilder.Entity<Job>();
            jobModel.Property(p => p.Id);
            jobModel.Property(p => p.ActionName).IsRequired(true);
            jobModel.Property(p => p.ActionParameters).IsRequired(false);
            jobModel.Property(p => p.EnqueueDate).IsRequired(true);
            jobModel.Property(p => p.ProcessingDate).IsRequired(false);
            jobModel.Property(p => p.ProcessingResult);
            jobModel.HasKey(c => c.Id);
            jobModel.ToTable("JobsQueue");

            var report = modelBuilder.Entity<ReportDetails>();
            report.Property(p => p.Id);
            report.Property(p => p.ReportId);
            report.Property(p => p.CreateDateTime);
            report.Property(p => p.Price);
            report.Property(p => p.PercentOfHigherPricesAtSameYear);
            report.Property(p => p.PercentOfHigherPricesAtDifferentYears);
            report.Property(p => p.AveragePrice);
            report.Property(p => p.AvgPriceSameYear);
            report.Property(p => p.AvgPriceSameYearSimiliarMileage);
            report.Property(p => p.AvgPriceSameYearSimiliarDisplacement);
            report.Property(p => p.AvgMileage);
            report.Property(p => p.ChartAvgPriceToYear);
            report.Property(p => p.ChartPricesOrdered);
            report.Property(p => p.ChartPricesSameYearOrdered);
            report.Property(p => p.PercentOfIsAutomaticClima);
            report.Property(p => p.PercentOfIsLeather);
            report.Property(p => p.PercentOfIsManualClima);
            report.Property(p => p.PercentOfIsMultifunctionalSteeringWheel);
            report.Property(p => p.PercentOfIsNavi);
            report.Property(p => p.PercentOfIsSeatHeating);
            report.Property(p => p.PercentOfIsStartStop);
            report.Property(p => p.PercentOfIsTempomat);
            report.Property(p => p.PercentOfIsXenon);
            report.Property(p => p.OtherCarsSameYearAndSimiliarPrice);
            report.Property(p => p.MainPriceFactors);

            report.HasKey(c => c.Id);
            report.ToTable("Reports");

            var payment = modelBuilder.Entity<Payment>();
            payment.Property(p => p.Id);
            payment.Property(p => p.ExpectedAmount);
            payment.Property(p => p.Amount).IsRequired(false);
            payment.Property(p => p.Commision).IsRequired(false);
            payment.Property(p => p.ConfirmationDateTime).IsRequired(false);
            payment.Property(p => p.CreateDateTime).HasDefaultValueSql("GETUTCDATE()");
            payment.Property(p => p.Description).IsRequired(false);
            payment.Property(p => p.Email).IsRequired(false);
            payment.Property(p => p.OperationDateTime).IsRequired(false);
            payment.Property(p => p.OriginalAmount).IsRequired(false);
            payment.Property(p => p.Status).HasDefaultValue(PaymentStatus.New);
            payment.Property(p => p.ReportRequestId);

            payment.HasKey(c => c.Id);
            payment.ToTable("Payments");

            var promoCode = modelBuilder.Entity<PromoCode>();
            promoCode.Property(p => p.Id);
            promoCode.Property(p => p.Code);
            promoCode.Property(p => p.ExpirationDate).IsRequired(false);
            promoCode.Property(p => p.PriceMultiple).IsRequired(false);
            promoCode.Property(p => p.PriceSingle).IsRequired(false);
            promoCode.Property(p => p.ReportRequestId).IsRequired(false);
            promoCode.Property(p => p.Type).IsRequired(true);

            promoCode.HasKey(c => c.Id);
            promoCode.ToTable("PromoCodes");
        }
    }
}
