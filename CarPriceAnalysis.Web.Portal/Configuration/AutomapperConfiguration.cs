﻿using AutoMapper;
using CarPriceAnalysis.Common.Business.Extensions;
using CarPriceAnalysis.Common.Model.Data;
using CarPriceAnalysis.Web.Portal.ViewModels;

namespace CarPriceAnalysis.Web.Portal.Configuration
{
    public class AutomapperConfiguration: Profile
    {
        protected override void Configure()
        {
            CreateMap<ReportRequest, ReportDetailsViewModel>()
                .ForMember(m => m.IsEnglishVersion, d => d.MapFrom(s => s.IsEnglishVersion == "Tak"))
                .ForMember(m => m.IsBroken, d => d.MapFrom(s => s.IsBroken == "Tak"))
                .ForMember(m => m.Price, d => d.Ignore());

            CreateMap<ReportDetails, ReportDetailsViewModel>()
                .ForMember(m => m.ChartData_AvgPriceToYear, d => d.MapFrom(s => s.ChartAvgPriceToYear.ToChartData()))
                .ForMember(m => m.ChartData_PricesOrdered, d => d.MapFrom(s => s.ChartPricesOrdered.ToLinearChartData(1000)))
                .ForMember(m => m.ChartData_PricesSameYearOrdered, d => d.MapFrom(s => s.ChartPricesSameYearOrdered.ToLinearChartData(1000)));

            CreateMap<CarDetailsViewModel, ReportRequest>();
            CreateMap<ReportRequest, DownloadDetailsViewModel>();
        }
    }
}
